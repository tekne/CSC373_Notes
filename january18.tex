\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini \\
\small{Based off notes by Professor Francois Pitt}}
\date{January 18 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclareMathOperator{\len}{len}

\begin{document}

\maketitle

\section{Minimum Spanning Trees: Continued}

Let's complete the algorithm from last time:
\begin{codebox}
  \Procname{\(\proc{Djikstra}(G, s, t)\)}
  \li \(Q \gets\) an empty priority queue, with the priority of \(v\) given by \(d[v]\)
  \li \(P = \varnothing\) (shortest paths spanning tree)
  \li \For \(v \in \attrib{G}{V}\) \Do
  \li   \(\pi[v] \gets \const{Nil}\) (the parent of \(v\) in \(P\))
  \li   \(d[v] \gets \infty\) (priority) \End
  \li   \(\proc{Enqueue}(Q, s)\)
  \li \(d[s] \gets 0\)
  \li Update the priority of \(s\) \\
  \Comment{Main loop}
  \li \While \(Q\) is not empty \Do
  \li   \(v = \proc{Dequeue}(Q)\)
  \li   \(P = P \cup \{v\}\) \Comment{A problem occurs when \(v = s\), which we handle below}
  \li   \For \((v, u) \in \attrib{G}{E}\) \Do
  \li     \If \(u \in Q\) and \(d[v] + w(v, u) < d[u]\) \Comment{``relaxation''} \Then
  \li       \(\pi[u] = v\)
  \li       \(d[u] = d[v] + w(v, u)\)
  \li       Update the priority of \(u\) \End\End\End
  \li \(P = P \setminus \{\const{Nil}, S\}\) \Comment{Cleanup}
  \li \Return P
\end{codebox}
We can now being our analysis of it's correctness and runtime.

\subsection{Runtime}
Roughly, we have that
\begin{itemize}

  \item Initialization takes \(\mc{O}(|V|)\) time

  \item The main loop iterates at most \(|V|\) times, since each iteration removes a vertex from the queue

  \item Each iteration examines the edges in one adjacency list and updates priorities, this taking \(\mc{O}(\log|V|)\) time.

\end{itemize}
Hence, we have that the algorithm runs in
\begin{equation}
  \mc{O}(|V| + (|V| + |E|)\log|V|) = \mc{O}(|E|\log|V|)
\end{equation}

\subsection{Correctness}

We begin by exploring a few properties of the problem
\begin{lemma}[Optimal substructure in in the Shortest Path problem]
  A subpath of a shortest path is also a shortest path
\end{lemma}
\begin{proof}
  We use the ``cut and paste technique.'' Assume we have a shortest path \(P\) from \(u\) to \(v\) and consider two vertices \(x\) and \(y\) along \(P\). Assume there exists a path \(Q\) between \(x\) and \(y\) shorter than the subpath of \(P\) between \(x\) and \(y\), denoted \(P_{x, y}\).

  Consider the path obtained by joining \(P_{u, x}, Q, P_{y, v}\). Clearly, this is a path from \(u\) to \(v\), and has length
  \begin{equation}
    \len(P_{u, x}) + \len(Q) + \len(P_{y, v}) < \len(P_{u, x}) + \len(P_{x, y}) + \len(P_{y, v}) = \len(P)
  \end{equation}
  contradicting the fact that \(P\) is a shortest path.
\end{proof}

\begin{lemma}[Triangle Inequality]
  Let \(G = (V, E)\) be a graph and \(\delta(x, y)\) denote the length of the shortest path between vertices \(x, y\).
  \begin{equation}
    \forall u, v, w \in V, \delta(u, v) \leq \delta(u, w) + \delta(w, v)
  \end{equation}
\end{lemma}
\begin{proof}
  The length of the shortest path between \(u\) and \(v\) by definition must be less than or equal to the length of the path between \(u\) and \(v\) obtained by gluing the length of the paths between \(u\) and \(w\) and the lengths of the paths between \(w\) and \(v\) (if there is no such path in any of these cases, we can use the fact that every extended real is \(\leq \infty\))
\end{proof}
We can now prove a lemma regarding Djikstra's algorithm, which will be useful in our proof of correctness.
\begin{lemma}
  During Djikstra's algorithm, \(d[v] \geq \delta(s, v)\), with this holding after any sequence of relaxations
\end{lemma}
\begin{proof}
  (Sketch) By induction: the inequality holds after the initialization step, since every vertex except \(s\) has \(d[v] = \infty\) and \(d[s] = 0 = \delta(s, s)\), and each relaxation preserves this property since after it, \(d[v]\) is either \(\infty\) or equal to the total weight of some path in \(G\).
\end{proof}

\subsubsection{Main Idea}
We want to show that, for all \(v \in P_i\), \(d[v] = \delta(s, v)\).
Consider one iteration of the main loop, with \(v\) being the vertex with minimum current \(d[v]\) removed from the queue and
\begin{equation}
  P_{i + 1} = P_i \cup \{(\pi[v], v)\}
\end{equation}
To show \(d[v] = \delta(s, v)\), we proceed by contradiction. Suppose \(d[v] > \delta(s, v)\) and let \(P\) be a path from \(s\) to \(v\) such that \(\len(P) = \delta(s, v)\). Assume \((x, y)\) is the first edge on \(P\) with \(x \in P_i, y \notin P_i\).
\begin{itemize}

  \item Assume \(y = v\). Then
  \begin{equation}
      d[v] = d[x] + w(x, v) = \delta(s, x) + w(x, v) = \delta(s, v) < d[v]
  \end{equation}
  giving a contradiction (\(d[v] < d[v]\))

  \item Assume \(y \neq v\). Then, where \((x, y)\) is one possible edge from \(P_i\) to \(y\),
  \begin{equation}
      d[y] \leq d[x] + w(x, y) = \delta(s, x) + w(x, y) < \delta(s, x) + w(x, y) + \delta(y, v) = \delta(s, v) < d[v]
  \end{equation}
  giving a contradiction (\(d[v] < d[v]\))

\end{itemize}
The rest of the proof involves adding induction structure to the main idea.

\subsubsection{Details}
TODO, if time

\end{document}
