\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini \\
\small{Based off notes by Professor Francois Pitt}}
\date{January 21-25 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclareMathOperator{\len}{len}

\begin{document}

\maketitle

\section{Dynamic Programming}

Let's begin with an example: activity scheduling with profits.

\subsection{Example: Activity Schedule With Profits}

This problem is the same as activity scheduling except for one crucial difference: each activity is associated with a ``profit'' and we want to schedule with maximum profit. More precisely, we have
\begin{itemize}

  \item Input: activites
  \begin{equation}
    \mc{A} = \{A_1 = (s_1, f_1, w_1),..., A_n = (s_n, f_n, w_n)\}
  \end{equation}
  where \(s_i, f_i, w_i\) are non-negative integer-valued start time, finish time (with \(f_i > s_i\)) and profit respectively

  \item Output: subset of activities \(S \subseteq \mc{A}\) such that no activites in \(S\) overlap and \(\proc{Profit}(S)\) is maximal.

\end{itemize}
Let's try a greedy approach:
\begin{enumerate}

  \item Greedy by finish time doesn't work: consider
  \begin{equation}
    \mc{A} = \{A_1 = (0.9, 1, 1), A_2 = (0.1, 0.9, 3), A_3 = (0, 0.1, 1)\}
  \end{equation}
  Our algorithm will pick \(A_1\) (being first), then skip \(A_2\) since it conflicts and choose \(A_3\), getting a profit of \(2\) instead of the maximum possible of \(3\) \label{maxunitprofit}

  \item Greedy by maximum profit doesn't work: consider
  \begin{equation}
    \mc{A} = \{A_1 = (0, 3, 2), A_2 = (0, 1, 1), A_3 = (1, 2, 1), A_4 = (2, 3, 1)\}
  \end{equation}
  The maximal subset is \(\{A_2, A_3, A_4\}\), but the algorithm will choose \(A_1\) and stop there since everything else overlaps with it.

  \item Greedy by max unit profit (profit/length) doesn't work: the counter example in \ref{maxunitprofit} gives the same incorrect result

  \item Combinations, for example trying to sort by finish time and then by max profit, still don't work. See again example \ref{maxunitprofit}.

\end{enumerate}
What do we try then? It turns out \textit{no} sorting strategy can make a greedy approach work for this problem. But does this mean we're back to brute force? Not quite.

Let's try sorting activities by finish time, as before. Consider now the optimal schedule \(S^*\). We have two possibilities: \(A_n \in S^*\) or \(A_n \notin S^*\).

\begin{itemize}

  \item If \(A_n \notin S^*\), then \(S^*\) trivially consists of the most optimal way to schedule \(A_1,...,A_{n - 1}^*\)

  \item If \(A_n \in S^*\), let \(k\) be the largest index such that \(A_k\) does not overlap with \(A_n\)
  Since we are sorting by finish time, if \(\ell < k\), then \(f_\ell < f_k \leq s_n\) implying \(A_\ell\) does not overlap with \(A_n\). On the other hand, if \(n \geq \ell > k\), then by definition \(A_\ell\) overlaps with \(A_k\). Hence the rest of \(S^*\) merely consistes of the most optimal way to schedule \(A_1,...,A_k\)

\end{itemize}
This hints at a recursive structure to the problem, which we can naively use to construct a recursive algorithm. Let's try computing the maximum profit we can gain. Assume \(\mc{A} = \{A_1,...,A_n\}\) is sorted, and that we, in precprocessing, construct an array \(p\) such that \(p[i]\) is the largest index such that \(A_{p[i]}\) does not overlap with \(A_i\) (or zero if there is no such index). We then obtain the following algorithm
\begin{codebox}
  \Procname{\(\proc{Recopt}(i)\)}
  \li \If \(i \isequal 0\) \Return \(0\)
  \li \Return \(\max(\proc{RecOpt}(n - 1), w_n + \proc{RecOpt}(p[n]))\)
\end{codebox}
Correctness is immediate from our above reasoning, but the runtime is exponential, as on any instance without too much overlap, we'll have exponentially many recursive calls (like recursive Fibonacci), roughly doubling each time.

However, in this case, there are only \(n + 1\) subproblems to solve: \(\proc{RecOpt}[0],...,\proc{RecOpt}[n]\). The exponential runtime is just due to wasted computation, recomputing things we already figured out.

One way to think of it is that, if greedy algorithms look like induction, as we only need a promising solution for \(n - 1\) to get a promising solution for \(n\), this situation, i.e. dynamic programming, looks like strong induction: we only need promising solutions to \(1,...,n - 1\) to get a promising solution for \(n\).

One thing we could do, then, is build up a vector of solutions to previous problems. This might, however, be inefficient: it could be that we don't need solutions to \textit{all} subproblems. It could also be that we can't clearly order the solutions in such a way that we know which ones we'll need first beforehand.

We can be even more clever and solve both these problems using \textit{memoization}. The idea here is to store the values in an array, computing the results of each recursive call only once. We fill up  the array with a \textit{sentinel} value, and each time we access the array to get the result of a recursive call, if we see the sentinel, we compute recursively, store and return, otherwise we just return, skipping the computation.

Let's try it: defining an array \(O\) of length \(n\) initialized to the sentinel value \(\const{Nil}\), we obtain algorithm
\begin{codebox}
  \Procname{\(\proc{MemRecOpt}(n)\)}
  \li \If \(O[n] \isequal \const{Nil}\) \Then
  \li   \(O[n] \gets \max( \proc{MemRecOpt}(n - 1), w_n + \proc{MemRecOpt}(p[n])\) \End
  \li \Return O[n]
\end{codebox}
The correctness result is the same as before, whereas we obtain runtime \(\mc{O}(n)\). To ``avoid the overhead of recursion'', we can use an iterative solution which computes the values in order, at the expense of computing vaues we may not need.

We can then reconstruct the optimal solution as follows:
\begin{codebox}
  \Procname{\(\proc{ReconstructSolution}(n)\)}
  \li \(S \gets \varnothing\)
  \li \(i \gets n\)
  \li \While \(i > 0\) \Do
  \li   \If \(O[i] = O[i - 1]\) \Then
  \li     \(i \gets i - 1\)
  \li   \Else
  \li     \(S \gets S \cup \{i\}\)
  \li     \(i \gets p[i]\) \End \End
  \li \Return S
\end{codebox}
The runtime of this is \(\Theta(n)\) if \(p[i]\) is precomputed and jobs are already sorted by finish time, otherwise, it's \(\Theta(n\log n)\). Let's generalize!

\subsection{The Dynamic Programming Paradigm}

We want to use dynamic programming for optimization problems that satsify the following properties:
\begin{itemize}

  \item ``Subproblem optimality'': an optimal solution to the problem can always be obtained from optimal solutions to subproblems

  \item ``Simple subproblems'': we can characterize subproblems precisely with a constant number of parameters, e.g. a few numerical indices.

  \item ``Subproblem overlap'': smaller subproblems are repeated many times in solving the problems of which they are components (for efficiency)

\end{itemize}
We can then apply the following 5 step plan:
\begin{enumerate}

  \item Figure out and describe the recursive structure of the problem you want to solve:
  \begin{itemize}

    \item How the problem can be decomposed into simple subproblems

    \item How a global solution relates to solutions to these subproblems

  \end{itemize} \label{describerec}

  \item Define an array indexed by the parameters which define subproblems to store the optimal value for each subproblem. Make sure that one of the ``sub''problems is actually the entire problem. \label{definearray}

  \item Based off the recursive structure obtained in step \ref{describerec}, describe a recurrence relation which the values (if all of them were filled in) of the array from step \ref{definearray} must satisfy. \label{describerecur}

  Note that in step \ref{definearray} we give meaning to the array, i.e. describe what the value stored in each location represents, whereas in step \ref{describerecur} we state a property of these values.

  \item Write an iterative algorithm to compute the values in the array, in a bottom-up fashion, following the recurrence from step \ref{describerecur}. We want to turn the relationship between array values into computation. \label{getiter}

  \item Use the computed array values to figure out the actual solution that achieves the best value. Generally, we want to modify the algorithm from step \ref{getiter} to find the answer, which may require storing additional information while we fill up the array.

\end{enumerate}
Let's apply this technique to another example

\subsection{Example: Matrix Chain Multiplication}

Given matrices \(A_0,...,A_{n - 1}\), there are many ways to parenthesize the matrix chain product
\begin{equation}
  \prod_{i = 0}^{n - 1}A_i
\end{equation}
All, barring numerical errors, will give the same answer by associativity, but not necessarily the same running time. For example, if \(A\) is \(1 \times 10\), \(B\) is \(10 \times 10\) and \(C\) is \(10 \times 100\), then \((AB)C\) takes on the order of \(1\cdot10\cdot10 + 1\cdot10\cdot100 = 1100\) operations, whereas \(A(BC)\) takes on the order of \(10\cdot10\cdot100 + 1\cdot10\cdot100 = 11000\) operations, an order of magnitude difference!

This gives rise to the Matrix Chain Multiplication problem:
\begin{itemize}
  \item Given: \(A_0,...,A_{n - 1}\) with dimensions \([d_0 \times d_1], [d_1 \times d_2], ...,[d_{n - 1} \times d_n]\) (given as a list \(D = [d_0,...,d_n]\))
  \item Find: the fully parenthesized product with the smallest total cost
\end{itemize}

How do we start? Brute force! The number of different ways to parenthesize the above expression is called a ``Catalan number'', and is \(\Omega(4^n)\) and hence not polynomial. Let's try some greedy approaches:

\begin{itemize}

  \item Sort such that the product with the smallest cost is first, or with the smallest dimension is eliminated first? Nope: consider \(D = [10, 1, 10, 100]\). The greedy approach gives \(10 \cdot 1 \cdot 10 + 10 \cdot 10 \cdot 100 = 10100\) operations, whereas we can find a solution taking \(1 \cdot 10 \cdot 100 + 10 \cdot 1 \cdot 100 = 2000\) operations.

  \item Sort such that the product with the smallest cost is last, or with the largest dimension is eliminated first? Nope: consider \(D = [1, 10, 100, 1000]\). The greedy approach takes \(10 \cdot 100 \cdot 1000 + 1 \cdot 10 \cdot 1000 = 1010000\) operations, whereas we can find a solution taking \(1 \cdot 10 \cdot 100 + 1 \cdot 100 \cdot 1000 = 101000\).

\end{itemize}
Indeed, we will again find that nothing works. So let's instead try our five step method:
\begin{enumerate}

  \item Structure of optimal subproblems: the main idea is that, instead of trying to find where to put the first product, we try to find out where to put the \textit{last} product. This is a common way of thinking when trying to come up with the recursive problem structure.
  For example, if we have \(A_0 \cdot \prod_{i = 1}^{n - 1}A_i\), then the last product consts \(d_0d_1d_n\), whereas if we have \(A_0A_1 \cdot \prod_{i = 2}^{n - 1}A_i\), the last product costs \(d_0d_2d_n\).

  Note that, in this case, there are only \(n - 1\) possibilities, since we merely have to pick which two adjacent matrices to put the product between. The information we need to find the best answer then is merely the best cost of doing each subproduct, since the best overall solution must include optimal subproducts (as otherwise we could improve on the best overall, giving a contradiction). \label{matstruct}

  \item Definiton of an array of subproblem values:
  \begin{itemize}

    \item We begin by clarifying what a subproblem is. From the recursive structure found in step \ref{matstruct}, we clearly want to consider arbitrary subproducts \(\prod_{k = i}^jA_k\).

    \item So, we define an \(n \times n\) array \(N\) such that \(N[i, j]\) is the smallest cost of multiplying \(A_i,...,A_j\).

    \item Then, from the structure of the optimal solution, the best way of doing \(\prod_{k = i}^jA_j\) is, for some \(i \leq \ell \leq j\),
    \begin{equation}
      \left(\prod_{k = i}^\ell A_k\right) \cdot \left(\prod_{k = \ell}^j A_k\right)
    \end{equation}
    where the left and right subproducts are done in the optimal way. \label{matarr}

  \end{itemize}

  \item Array recurrence: from the reasoning in step \ref{matarr}, we have
  \begin{itemize}
      \item \(\forall i, N[i, i] = 0\)
      \item \(\forall i < j, N[i, j] = \min\{ d_id_kd_{j + 1} + N[i, k - 1] + N[k , j] : i < k \leq j\}\)
  \end{itemize}

  \item Bottom-up algorithm:
  \begin{itemize}

    \item The basic recursive solution suffers from combinatorial explosion, i.e. subproblems are often recomputed multiple times, yielding exponential runtime, even though only \(n^2\) values must be computed in total

    \item Instead of recomputing, we can compute smaller values first and store them in the array, eliminating the need for recursive calls. This, however, gives rise to the constraint that

    \item We must compute values such that all entries \(N[i, k - 1], N[k, j]\) are present before \(N[i, j]\) is computed.

  \end{itemize}
  One possible implementation is then given by
  \begin{codebox}
    \Procname{\(\proc{MatrixChain}(d, n)\)}
    \li \For \(i \gets n - 1,...,0\) \Do
    \li   \(N[i, i] \gets 0\)
    \li   \For \(j \gets i + 1,...,n - 1\) \Do
    \li     \(N[i, j] \gets \infty\)
    \li     \For \(k \gets i + 1,...,j\) \Do
    \li       \(t \gets d[i]d[k]d[j + 1] + N[i, k - 1] + N[k, j]\)
    \li       \If \(t < N[i, j]\) \Then
    \li         \(N[i, j] \gets t\) \End\End\End\End
    \li \Return \(N[0, n - 1]\)
  \end{codebox}
  This has a trivial running time of \(\Theta(n^3)\).

  \item Reconstructing the solution: once we have our values, how can we find the actual parenthesized expression?

  One possibility is, working from \(N[0, n - 1]\), to recompute all possibilities previously considered in order to find the breakpoint \(k\) that yielded the best value, recursively doing the same for each subproblem. This, of course, would require an additional \(\Theta(n^3)\) time. Instead, however, we can trivially modify the algorithm use another array \(B[i, j]\) to store the value of \(k\) used. This gives
  \begin{codebox}
    \Procname{\(\proc{MatrixChain}(d, n)\)}
    \li \For \(i \gets n - 1,...,0\) \Do
    \li   \(N[i, i] \gets 0\)
    \li   \(B[i, i] \gets i\)
    \li   \For \(j \gets i + 1,...,n - 1\) \Do
    \li     \(N[i, j] \gets \infty\)
    \li     \For \(k \gets i + 1,...,j\) \Do
    \li       \(t \gets d[i]d[k]d[j + 1] + N[i, k - 1] + N[k, j]\)
    \li       \If \(t < N[i, j]\) \Then
    \li         \(N[i, j] \gets t\)
    \li         \(B[i, j] \gets k\)
    \End\End\End\End
    \li \Return \(N[0, n - 1]\)
  \end{codebox}

  The solution can then trivially be extracted from \(B\) in linear time.

\end{enumerate}

\end{document}
