\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{February 11-15 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}

\begin{document}

\maketitle

\section{Complexity of Ford-Fulkerson}

With our current understanding, it is possible for the runtime of the algorithm to depend on capacity values in the graph. This means, however that we might not have an algorithm that always runs in polynomial time.
Let's make some attempts to fix this.
\begin{enumerate}

  \item Use breadth first search (BFS) to find augmenting paths.

  We can prove, if we do this, that we need at most \(\mc{O}(|V||E|)\) augmentations. Since BFS runs in time \(\mc{O}(|E| + |V|) = \mc{O}(|E|)\), the total time taken is hence \(\mc{O}(|V||E|^2)\).

  This is fine, since it's polynomial time, though it's not great, since the number of edges can be all the way up to \(n^2\), giving us runtime as bad as \(\mc{O}(n^5)\). So for practical reasons, we would like to do a little better if we can.

  It turns out, the tricky step in proving that this works is proving that at most \(\mc{O}(|V||E|)\) augmentations are necessary, which is not obvious. But we can improve this as follows: right now, every time we find an augmenting path, we run BFS from scratch. But when we run BFS once, we get a lot more information than just one path from source to sink: we get a whole traversal tree in the network, which may include more than one augmented path.

  What if we run BFS once and \textit{then} loop over the paths in that BFS tree. Doing that well, and doing that in a way that actually improves the running time, is not obvious, so we're not going to go over the details. But let's write it down: \label{bfspath}

  \item Idea \ref{bfspath} but with each execution, using all augmented paths found before running BFS again. The details get even trickier here, but it's possible to use this to improve the running time to \(\mc{O}(|V|^2|E|) = \mc{O}(n^4)\). \label{usebfsright}

  But can we do better? It turns out there are other techniques that are completely different. It's not clear that we can squeeze more out of ideas like \ref{bfspath} and \ref{usebfsright}, but it turns out that

  \item \(\mc{O}(n^3)\) is possible using a different technique called ``preflow algorithms'', which is pretty much state of the art. I'm mentioning it so that you're aware of it, but the idea is that all our algorithms so far have been based of Ford-Fulkeson and the assoiated invariant, whereas in this case, we deliberately break that invariant and then correct. I'm deliberately omitting the details, but I want you to be aware that this is possible, without worrying about the details.

\end{enumerate}
You might be thinking: ``why not?'' Isn't this an algorithms course? Shouldn't I be talking about the algorithms and the details?

Well, that's right, except this problem is a little different, and hence there's a reason I'm not going into the details. That is, in practice, this topic, network flow, and the next topic, linear programming, are different than the previous topics we discussed. Network flow and linear programming are in a sense generic problems that we can use to solve \textit{other} problems. Whereas greedy algorithms and dynamic programming require coming up with new algorithms for every problem, tailored to the particular problem we're trying to solve, when we're using network flow or linear programming, we don't come up with a new algorithm every time. Instead, we take a problem, transform it into a network flow problem, solve that, and return.

So we abstract away the specific network flow implementation, it's just a tested, optimized ``library'' function we make use of when we can. Let's do some examples

\section{Transformation to Network Flow}

\subsection{Maximum Bipartite Matching}

Assume we are given as an input a bipartite graph \(G = (V_1, V_2, E)\), i.e. with all edges \((u, v) \in E\), \(u \in V_1, v \in V_2\) (with \(V_1, V_2\) disjoint). We want to find what's called a ``matching'': a set \(M\) of edges so that no two edges that I select have an endpoint in common, in such a way that I have as many edges as possible, i.e. maximizing \(|M|\).

Can we solve this with network flows? Well, since it's in this section... So, how do we start? We have to try to transform the problem into a network: we begin by making all the edges into directed edges from \(V_1\) to \(V_2\). We then want to give all the edges a capacity, set to \(1\). Now we need a source and a sink, so here's what we're going to do. We're going to add a source \(s\) and a sink \(t\) and link them to every vertex in \(V_1\), \(V_2\) respectively with a capacity of \(1\).

\subsubsection{Algorithm}

Let's write this down more formally:
\begin{enumerate}

  \item Define a network \(N = (V, E')\) where:
  \begin{itemize}
    \item \(V = \{s, t\} \cup V_1 \cup V_2\)
    \item \(E' = \{(s, u) : u \in V_1\} \cup \{(v, t) : v \in V_2\} \cup E\)
    \item \(c: E' \to \reals^+, c(e') = 1\)
  \end{itemize}

  \item Find maximum flow \(f\)

  \item Output \(M = \{(u, v) \in E : f(u, v) = 1\}\)

\end{enumerate}

\label{maximummatch}

\subsubsection{Correctness}

\begin{itemize}

  \item For any matching \(M \in G\), there is a corresponding flow \(f\) over \(N\), given by, for \(u \in V_1, v \in V_2\),
  \begin{equation}
    f(u, v) = \left\{\begin{array}{cc}
      1 & \text{if } (u, v) \in M \\
      0 & \text{otherwise}
    \end{array}\right.,
    f(s, u) = \left\{\begin{array}{cc}
      1 & \text{if } u \text{ is matched}\\
      0 & \text{otherwise}
    \end{array}\right.,
    f(v, t) =  \left\{\begin{array}{cc}
      1 & \text{if } v \text{ is matched}\\
      0 & \text{otherwise}
    \end{array}\right.
  \end{equation}
  This implies the maximum flow is greater than or equal to the max matching size. For correctness, then, we need to show that

  \item For any valid flow \(f\), there is a matching
  \begin{equation}
    M = \{(u, v) \in E : f(u, v) = 1\}
  \end{equation}


\end{itemize}

\subsection{Comparison With Other Techniques}

When we solve optimization problems with ``network flow techniques'', what we are really doing is \textit{transforming} or \textit{reducing} our problem to finding the maximum flow in some network, then using a standard algorithm to do so.
The algorithm and problem don't change, only the input changes, and hence the core of the problem moves from figuring out how to solve the problem itself to figuring out how to generate inputs and interpret outputs so as to reconstruct a solution.

Because of this, the algorithm we end up with will always have the following structure:
\begin{enumerate}

  \item Describe how to transform an instance \(p\) of the problem to a network \(N\)

  \item Find a maximum flow \(f\) (or a minimum cut \(X\)) for \(N\) using the standard techniques

  \item Use \(f\)/\(X\) to reconstruct a solution to \(p\)

\end{enumerate}
There are two ways in which we can fail here:
\begin{itemize}

  \item There exist valid solutions which do not correspond to a flow or cut in the network constructed. We can show this is not the case by proving that every valid solution yields a valid flow or cut in the network (or, if we only need one solution, show that the existence of a valid solution implies the existence of a corresponding valid flow or cut)

  \item There exist flows, \textit{integer} flows or cuts in the network which do not correspond to valid solutions. We show this is not the case by proving that every flow/integer flow/cut corresponds to a valid solution.

\end{itemize}
It is often the case that proofs of these properties are very short as the correspondence between solutions and flow/cuts/integer flows is obvious. Nevertheless, both parts are of vital importance and must always be included.

\subsection{Maximum Independent Set in a Bipartite Graph}

We now move on to consider another application of network flows: finding the maximum independent set in a given bipartite graph. The problem is defined as follows:
\begin{itemize}

  \item Input: a bipartite graph \(G = (V_1, V_2, E)\)

  \item An independent set \(I \subseteq V_1 \cup V_2\) maximizing \(|I|\), where \(I\) is said to be ``independent'' if and only if no edge has both endpoints in \(I\).

\end{itemize}

\subsubsection{Algorithm}

\begin{enumerate}

  \item Create a network \(N\) from \(G\) by adding a source \(s\), a sink \(t\) and edges with capacity \(1\) from \(s\) to every vertex in \(V_1\) and from every vertex in \(V_2\) to \(t\). Make the original edges directed from \(V_1\) to \(V_2\) and have capacity \(\infty\). Note the only difference between this setup and that for Maximum Bipartite Matching (\ref{maximummatch}) is that here the edges between \(V_1\) and \(V_2\) have infinite capacity.

  \item Find a cut \(X = (V_s, V_t)\) with minimum capacity. We can do this by finding a maximum flow and then running the algorithm used in the proof of the Ford-Fulkerson theorem to obtain a minimum cut.

  \item Return
  \begin{equation}
    I = (V_1 \cap V_s) \cup (V_2 \cap V_t)
    \label{defi}
  \end{equation}

\end{enumerate}
Let's elaborate on why this is correct:

\subsubsection{Correctness}

\begin{itemize}

  \item Let \(I\) be independent, and define
  \begin{equation}
      V_s = \{s\} \cup \{V_1 \cap I\} \cup (V_2 \setminus I), V_t = (\{t\} \cup V_1 \cup V_2) \setminus V_s
  \end{equation}
  and consider the cut \(X = (V_s, V_t)\). The only edges across this cut are those from \(s\) to vertices in \(V_1 \setminus I\) and from vertices in \(V_2 \setminus I\) to \(t\). Hence,
  \begin{equation}
    c(X) = |V_1| + |V_2| - |I|
  \end{equation}
  If \(I^*\) is the maximum independent set, then
  \begin{equation}
    |I| \leq |I^*| \implies c(X) \geq |V_1| + |V_2| - |I^*|
  \end{equation}
  i.e., if \(X^*\) is the minimum capacity cut, we have
  \begin{equation}
    c(X^*) \leq |V_1| + |V_2| - |I^*| \iff |I^*| \leq |V_1| + |V_2| - c(X^*)
  \end{equation}

  \item Let \(X = (V_s, V_t)\) be a cut with finite capacity, and define \(I\) as in the algorithm (Equation \ref{defi}). As \(c(X)\) is finite, there is no edge \((u, v)\) in the original graph with \(u \in V_s, v \in V_t\), as all such edges have infinite capacity by construction (implying the cut would have infinite capacity). Therefore, it follows that \(I\) is an independent set by construction, since it only contains elements of \(V_s, V_t\) which are not \(\{s, t\}\) and hence have no edges between them.

  Furthermore, every edge across \(X\) is either from \(s\) to \(V_1 \setminus I\) or from \(V_2 \setminus I\) to \(t\), giving
  \begin{equation}
      c(X) = |V_1| + |V_2| - |I| \geq |V_1| + |V_2| - |I^*| \implies c(X^*) \geq |V_1| + |V_2| - |I^*|
  \end{equation}

\end{itemize}
Hence we have
\begin{equation}
    c(X^*) = |V_1| + |V_2| - |I^*|
\end{equation}
implying specifically that the set \(I\) obtained from the minimum cut \(c(X^*)\) is of maximal size, completing the proof of correctness.

\end{document}
