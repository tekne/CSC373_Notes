\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{February 18-22 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

\section{Linear Programming}

Let's, as always, begin with some examples:

\subsection{The Political Advertising Problem}

Consider a politician with four different platforms:
\begin{itemize}
  \item Building roads
  \item Gun control
  \item Farm subsidies
  \item Gasoline tax
\end{itemize}
The politician wants to advertise to three different ridings: urban, suburban and rural. The catch is that advertisement can only be done ``globally'', that is, all advertising is seen by all three ridings. The politician peforms market research to find out how many voters are gained or lost for each dollar spent on advertising each platform, and comes up with Figure \ref{fig:politician}.

\begin{figure}
\caption{Number of voters gained or lost per dollar spent on platform}
\begin{center}
\begin{tabular}{c|ccc}
    & Urban & Suburban & Rural \\
  \hline
  Roads & -2 & 5 & 3 \\
  Guns & 8 & 2 & -5 \\
  Farm & 0 & 0 & 10 \\
  Gas & 10 & 0 & -2
\end{tabular}
\end{center}
\label{fig:politician}
\end{figure}

The politician wants to gain at least \(50,000\) urban voters, \(100,000\) suburban votes and \(25,000\) rural voters. Our task is to try to find out how to spend as little as possible on advertising to gain the required number of votes for each riding. Let's think about how we can represent the problem.

We start by introducing four variables \(x_1, x_2, x_3, x_4\), defined as follows:
\begin{itemize}
  \item \(x_1 = \) thousands of dollars to advertise on building roads
  \item \(x_2 = \) thousands of dollars to advertise on gun control
  \item \(x_3 = \) thousands of dollars to advertise on farm subsidies
  \item \(x_4 = \) thousands of dollars to advertise on gasoline tax
\end{itemize}
We want to spend as little as possible, i.e. minimize \(x_1 + x_2 + x_3 + x_4\), subject to the constraints that:
\begin{itemize}
  \item We need at least \(50,000\) urban voters, i.e. \(-2x_1 + 8x_2 + 0x_3 + 10x_4 \geq 50\)
  \item We need at least \(100,000\) suburban voters, i.e. \(5x_1 + 2x_2 + 0x_3 + 0x_4 \geq 100\)
  \item We need at least \(25,000\) rural voters, i.e. \(3x_1 - 5x_2 + 10x_3 - 2x_4 \geq 25\)
\end{itemize}
One more thing: numerically, we can't spend negative amounts, and hence \(x_1, x_2, x_3, x_4 \geq 0\). This is known as a linear program

\subsection{Linear Programs}

In general, a linear program is an optimization problem consisting of
\begin{itemize}

  \item Real-valued variables \(x_1,...,x_n\)

  \item An ``objective function''
  \begin{equation}
    \sum_{i = 1}^nc_ix_i
  \end{equation}
  where each \(c_i\) is a real constant

  \item Linear ``constraints'', i.e. equations of the form
  \begin{equation}
    \sum_{j = 1}^na_{i, j}x_j \ \ \leq \text{ or } = \text{ or } \geq \ \ b_i
  \end{equation}
  for \(i = 1, 2,..., m\), where each \(a_{i,j}, b_i\) is a real constant. Often written
  \begin{equation}
    \mb{A}x \ \ \leq \text{ or } = \text{ or } \geq \ \ \mb{b}
  \end{equation}
  where \(\mb{A} = (a_{i, j}) \in \reals^{m \times n}\), \(\mb{b} = (b_i) \in \reals^m\).

\end{itemize}
The goal is simple: find real values for the \(x_i\)'s which maximize or minimize the objective function and satisfy all constraints. ``Integer programming'' is a restricted subset of this problem in which only integer solutions are considered, which is NP-hard (i.e. there is no efficient algorithm, unless of course P = NP).

\subsubsection{Solving Linear Programs}

The ``feasible region'' is the set of values for \(x_1,...,x_n\) which satisfy all constraints. It can be:
\begin{itemize}

  \item Empty: e.g. for the constraints \(x_1 + x_2 \leq -1\) and \(2x_1 + 2x_2 \geq 5\), which no numbers \(x_1, x_2\) can  simultaneously satisfy. In this case, the linear program has no solutions.

  \item Unbounded: e.g. for the constraints \(x_1, x_2 \geq 0\) and \(x_1 + x_2 \geq 10\) with the objective function maximizing \(x_1, x_2\), we can pick arbitrarily large \(x_1, x_2\) and hence there is no optimal solution to the linear program.

  \item Bounded: e.g. for the constraints \(x_1, x_2 \geq 0\) and \(x_1 + x_2 \leq 10\), there are either one or infinitely many solutions to the linear program, depending on the objective function.

\end{itemize}
We have the following methods available to us to try to explore the feasible region in search of solutions:
\begin{itemize}

  \item The simplex method, intuitively, moves from vertex to vertex of the boundary of the feasible region using algebraic manipulations similar to how one would solve a linear system using Gaussian elimination. In the worst case it takes exponential time, but in practice it is quite efficient.

  \item ``Interior point'' methods, on the other hand, take worst-case polynomial time, but only recently have they begun to catch up to simplex methods in terms of real-world efficiency.

\end{itemize}

\subsection{Applications of Linear Programming}

\subsubsection{Network Flows}

Given a network \(N = (V, E)\) with capacities \(c(e)\) for each edge \(e \in E\), we can construct a linear program with variables \(f_e\) for each \(e \in E\), having constraints
\begin{equation}
  \forall e \in E, 0 \leq f_e \leq c(e)
\end{equation}
\begin{equation}
  \forall v \in V, \sum_{(u, v) \in E}f_{(u, v)} - \sum_{(v, u) \in E}f_{(v, u)} = 0
\end{equation}
and objective function (to maximize)
\begin{equation}
  \sum_{e \in E}f_e
\end{equation}
This is a direct restatement of the network flow problem. Unsurprisingly, since linear programming is hence ``more general,'' we can't solve network flow more efficiently in this case. Another catch is that, unlike with the Ford-Fulkerson algorithm, we cannot guarantee integer solutions given integer weights.

\subsubsection{Shortest \(s-t\) path}

Given a graph \(G = (V, E)\) with weights \(w(e)\) for each edge \(e\), we can construct a linear program with variables \(d_v\) for each vertex \(v\) with objective function (to maximize) \(d_t\), subject to the constraints
\begin{equation}
  \forall (u, v) \in E, d_v \leq d_u + w(u, v)
\end{equation}
\begin{equation}
  d_s = 0 \land \forall v \in V, d_v \geq 0
\end{equation}
We can't use minimizing \(d_t\) as an objective, since that would allow the algorithm to assign values for each \(d_v\) lower than their actual values. However, if we try to \textit{maximize} \(d_t\) instead, then the constraints force it to be no more than the shortest distance, whereas the maximization forces it to be no less, and hence, we find the true shortest distance.

\subsubsection{Minimum Vertex Cover}

We begin by stating the problem of finding a minimum vertex cover:
\begin{itemize}

  \item Input: an undirected graph \(G = (V, E)\)

  \item Output: a subset of vertices \(C \subseteq V\) that ``covers'' each edge, i.e.
   \begin{equation}
     \forall (u, v) \in E, \{u, v\} \cap C \neq \varnothing
   \end{equation}
   (at least one of the endpoints is in \(C\)) of minimal size.

\end{itemize}

We can represent this an integer problem, with a variable \(x_v\) for each vertex \(v \in V\), with objective function (to minimize)
\begin{equation}
  \sum_{v \in V}x_v
\end{equation}
subject to the constraints
\begin{equation}
  \forall (u, v) \in E, x_u + x_v \geq 1
\end{equation}
\begin{equation}
  \forall v \in V, 0 \leq x_v \leq 1
\end{equation}
Any vertex cover \(C\) yields a \textit{feasible} solution, as if we set \(x_v = 1\) if \(v \in C\) and \(x_v = 0\) otherwise,
\begin{equation}
  \forall (u, v) \in E, \{u, v\} \cap C \neq \varnothing \implies x_u = 1 \lor x_v = 1 \implies x_u + x_v \geq 1
\end{equation}
and similarly, any feasible solution yields a vertex cover given by
\begin{equation}
  C = \{v \in V : x_v = 1\}
\end{equation}
since again
\begin{equation}
  \forall (u, v) \in E, x_u + x_v \geq 1 \implies x_u = 1 \lor x_v = 1 \implies \{u, v\} \cap C \neq \varnothing
\end{equation}
Unfortunately, as integer programming is NP hard, the problem cannot be solved in polynomial time using this formulation.

\section{P and NP}

\subsection{Review and Overview}
So far, we've looked at
\begin{itemize}

  \item Problems which can be solved by ``simple'' algorithms, like greedy algorithms

  \item Problems which cannot be solved efficiencly by simple techniques but can be solved efficiencly using more complex algorithms and techniques such as limited backtracking, dynamic programming, network flow and now linear programming.

  \item Problems which cannot be solved efficiently by any known algorithm, as the only known solutions involve exhaustive search (brute force) or unlimited backtracking.

\end{itemize}
Given a problem to solve, we would like to have
\begin{itemize}

  \item A method to, in general, find the best algorithmic method to solve a problem. For certain classes of problems, this is possible (e.g. ``matroid theory'' for greedy algorithms) but in general only trial and error experience can help.

  \item A method to, in general, prove a given problem has no efficient solution. Again, we can do this in some specific cases, but in the majority of cases, no guarantees are known. However, as we shall see, we can get close.

\end{itemize}

\subsection{Running Times and Input Size}

So as to be able to discuss problem complexity, we need to be more precise about the model of computation we are using (RAM model, Turing machines, recursive functions, etc.) and how exactly we want to measure input size. To be more precise, we define the ``standard size'' of an object to be the total number of bits required to write down the object using a ``reasonable'' encoding. For example:
\begin{itemize}

  \item For a graph \(G\) represented as an adjacency matrix, we require an \(n \times n\) array of bits, and hence \(n^2\) bits, giving \(\size(G) = n^2\)

  \item An integer \(x\) stored as a sequence of bits requires \(\floor{\log_2(x + 1)} \approx \log_2x\) bits, and hence
  \(\size(x) = \log_2x\)

  \item A list of integers \([x_1,...,x_n]\) needs at least \(\size(x_1),...,\size(x_n)\) bits to write down, or if we use the same number of bits for each,
  \begin{equation}
    \size([x_1,...,x_n]) = n\log_2(\max(x_1,...,x_n))
  \end{equation}
  If we additionally assume that integers always take up a constant number of bits, then this size becomes \(n\) times a constant. That said, this assumption does not always hold.

\end{itemize}
It turns out that different models of computation and methods of measuring input size affect running time by at most a polynomial factor (i.e. doubling, squaring, etc.). So for example, a problem solved in \(\Theta(n)\) in one model can take \(\Theta(n^3)\) in another, but not, say, \(\Theta(2^n)\). There are, however, two important exceptions to keep in mind:
\begin{itemize}

  \item Storing integers in ``unary'' format, that is, storing \(n\) with \(n\) \(1\)'s, requires exponentially more space than any other base (including binary format), so this is ruled out.

  \item Non-deterministic models of computation \textit{appear} to solve certain problems exponentially faster than deterministic ones, though this is not yet proven (we do not know if P = NP) and subject to some important caveats

  \item With that in mind, quantum computation also affects runtime in strange ways, but that is beyond the scope of this course.

\end{itemize}
With \(\mc{O}, o\) and \(\Theta\) notation, we don't have a way to deal with differences of more than a constant factor. But we can do so by using a coarser scale: ignoring polynomial differences. Any polynomial time algorithm as a function of some informal ``size'' is then polynomial time as a function of size in terms of bits (``bitsize''), so long as we only use ``reasonable'' primitive operations.
\begin{itemize}

  \item Comparisons and bitwise operations are reasonable, as are additions and subtractions

  \item Multiplication and exponentiation are \textit{not} reasonable operations, since they can make the size of integers grow exponentially. Of course, this doesn't mean we can't \textit{use} multiplication and exponentiation, just that we can't simply count it as a primitive operation like we would do addition.

\end{itemize}

\subsection{Towards P and NP}

Intuitively,
\begin{itemize}

  \item \(P = \) all problems which are solvable by a \textit{deterministic} algorithm in polynomial time

  \item \(NP = \) all problems which are solvable by a \textit{non-deterministic} algorithm in polynomial time \( = \) all problem which have solutions that can be \textit{verified} in polynomial time

\end{itemize}
Formal definitions will be provided next week.

\end{document}
