\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 11 - March 15 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}
\usepackage{xcolor}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ptrans}[0]{\xrightarrow{\Pt}}
\newcommand{\TODO}[1]{\textcolor{red}{\textbf{TODO:} #1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclareMathOperator{\Pt}{P}
\DeclareMathOperator{\NPt}{NP}
\DeclareMathOperator{\coNP}{coNP}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

\section{Template for Proofs of \(\NPt\)-completness}

To show that \(A\) is \(\NPt\)-complete, prove that:
\begin{itemize}

  \item \(A \in \NPt\): describe a polynomial time verifier for \(A\), of the form ``Given \(x, c\) describe the type of \(c\) and check that \(c\) has the desired properties.'' Argue the verifier runs in polynomial time and that \(x\) is a yes-instance if and only if the verifier outputs ``yes'' for some \(c\).

  Note that all the problems we've seen so far in \(\NPt\) share a similar structure: their definition is of the form ``the answer for object \(A\) is Yes if and only if there exists some object \(B\) such that some property \(P\) holds for \(A\) and \(B\)''. For example, for \(\proc{Clique}\), we can substitute in
  \begin{itemize}

    \item \(A = \) a graph \(G = (V, E)\) and a positive integer \(k\)

    \item \(B = \) a subset \(C \subset V\)

    \item \(P = \) \(|C| = k\) and \(\forall u, v \in C, (u, v) \in E\)

  \end{itemize}
  Because of the way these decision problems are defined, we are guaranteed that \((A, c)\) is accepted for some \(c\) (where \(c\) encodes \(B\)) if and only if \(A\) is a yes-instance, and hence all we have to do is ensure \(P\) can be checked in polynomial time.

  \item \(A\) is \(\NPt\)-hard: that, is, the hard part. The general idea, though, isn't so difficult: all we need to do is show that \(B \ptrans A\) for some \(\NPt\)-hard problem \(B\). These arguments generally take the form ``given \(x\) (an input to \(B\)), construct \(y_x\) (an input to \(A\)) as follows...''

  We need to argue that construction can be carried out in polynomial time and that \(x\) is a yes instance if and only if \(y_x\) is a yes-instance. This is often done by cases, that is, showing implication both ways. To recap, we need to
  \begin{enumerate}

    \item Starting with an arbitrary input \(x\) for \(B\) (without specifying whether \(x \in B\) or \(x \notin B\))

    \item Describe an explicit construction of an input \(y_x\) for \(A\) from \(x\), which we must argue

    \item Can be constructed in polynomial time, and

    \item That if \(y_x\) is a yes-instance, \(x\) is, and

    \item that if \(x\) is a yes-instance, \(y_x\) is

  \end{enumerate}
  Watch the last step! The argument starts from the \(y_x\) constructed earlier, not some arbitrary input \(y\) for \(A\), and relates it to the arbitrary \(x\) from the first step (that \(y_x\) was constructed from in the second)

  There are a few pitfalls to watch out for when constructing these arguments, some of which are
  \begin{itemize}

    \item The direction of reduction: start with an arbitrary input \(x\) \underline{for \(B\), not \(A\)}, and explicitly construct a specific input \(y_x\) for \(A\). The reduction must work with all possible inputs: no restrictions allowed!

    \item We can't have a ``reduction''' that does something different for yes-instances and no-instances, as that would involve telling the difference, which takes \(\NPt\)-time, and of course we don't know if \(\Pt = \NPt\).

    \item We can't have a ``reduction'' which makes use of a certificate \(c\) for \(x\): if \(B\) is \(\NPt\)-complete, we only know a certificate \textit{exists}, not that we can find one in \(\Pt\), and the reduction must start from \(x\) alone: no certificate!

  \end{itemize}

\end{itemize}

\subsection{\(\proc{Sat} \ptrans \proc{3Sat}\)}

\TODO{copy}

\section{Self-Reducibility}

We begin by noting that the summary in this section will be more detailed than usual as this material is not explicitly covered in the textbook, only showing up in exercises. So far, we've focused on decision problems, but many problems are much more naturally posed as ``search problems'' of the general form ``given input \(x\), find solution \(y\)''. Some examples of problems we've looked at before in terms of search problems include:
\begin{enumerate}[label=(P\arabic*)]

  \item \(\proc{Sat-Search}\): Given a propositional formula \(\phi\), find a satisfying assignment, if one exists

  \item \(\proc{Clique-Search}\): Given a graph \(G\) and an integer \(k\), find a \(k\)-clique, if one exists

  \item \(\proc{HamPath-Search}\): Given a graph \(G\), find a Hamilton path in \(G\), if one exists

  \item \(\proc{Subset-Sum-Search}\): Given a set of numbers \(S\) and a target \(t\), find a subset \(S' \subseteq S\) whose sum is \(t\), if one exists

\end{enumerate}

\begin{definition}

  We say that \(A\) is \textbf{Turing-reducible} to \(B\) in polynomial time iff \textbf{assuming} (this does not actually have to be the case) we have an algorithm \(S_B\) which solves \(B\) in constant time (an ``\textbf{oracle}'') we can write an algorithm \(S_A\) to solve \(A\) which runs in at most polynomial time (by making appropriate calls to \(S_B\)).

\end{definition}
We reiterate that this does \textbf{not} require that \(B\) is actually solvable in constant time or even polynomial time, only that, if we had some ``magical machine'' that would tell us the answer to \(B\) in a single ``instruction'' (the oracle), we could write a program using this machine to compute \(A\) in polynomial time. So in a way, \(A\) takes polynomial time + polynomial applications of \(B\).

Back to the task at hand, it is clear that an efficient solution to a search problem gives an efficient solution to the corresponding decision problem. For example, if we have a ``magical solution'' to \(\proc{Clique-Search}\), then we could easily get a solution to \(\proc{Clique}\) as follows:
\begin{codebox}
  \Procname{\(\proc{Clique}(G, k)\)}
  \li \Return \(\proc{Clique-Search}(G, k) \neq \const{Nil}\)
\end{codebox}

So if we prove the decision problem is \(\NPt\)-hard, the search problem is ``\(\NPt\)-hard'' as well (in some generalized sense of the term) and does not have an efficient solution (if \(\Pt \neq \NPt\)). It turns out, however, that this \textbf{often} goes the other way around: decision problems are often only ``polynomially harder'' than their corresponding search problems. That is, the search problem is often Turing-reducible to the associated decision problem. When this happens, it is called ``self-reducibility.'' Let's look at some examples:

\subsection{\proc{Clique-Search}}

\begin{claim}
  \(\proc{Clique-Search}\) is Turing-reducible to \(\proc{Clique}\) in polynomial time
\end{claim}
\begin{proof}
  \TODO{Fill in}
\end{proof}

\subsection{\proc{HamPath-Search}}

\TODO{copy}

\subsection{\proc{Vertex-Cover-Search}}

\TODO{copy}

\subsection{Optimization Problems}

\TODO{copy}

\subsubsection{\proc{Max-Clique}}

\TODO{copy}

\end{document}
