\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 14 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}

\begin{document}

\maketitle

This week, there's two topics I'm going to talk about, both under the umbrella of greedy algorithms: minimum spanning trees and shortest paths. We're not going to go into the details of how to solve this, since you've already done this in 263/265, so I'm just going to remind you briefly.

\section{Minimum Spanning Trees}

So here's the problem:
\begin{itemize}

  \item Given input of a weighted graph \(G = (V, E)\) with \(\forall e \in E, w(e) \in \nats\) such that \(G\) is connected

  \item We want to compute a spanning tree. There's lots of ways to represent it, but conceptually all we really want is a subset of the edges \(T \subseteq E\) with minimum \(w(T) = \sum_{e \in T}w(e)\).

\end{itemize}
Reminders:
\begin{itemize}

  \item A spanning teee is an acyclic, connected subset of edges that spans \(V\), i.e. reaches every \(v \in V\).

  \item If \(|v| = n\), then \(|T| = n - 1\).

  \item For an spanning tree \(T\) and edge \(e \notin T\), \(T \cup \{e\}\) contains exactly one cycle

  \item For any spanning tree \(T\) and edge \(e \in T\), \(T - \{e\}\) consists of exactly two connected components.

\end{itemize}
So what's the first algorithm we want to look at? Brute force! But there are \(2^|E|\) such subsets, so even if it takes \(\mc{O}(1)\) to check each one... So we're not even going to bother writing it down. Moving on...

\subsection{Prim's Algorithm}

We're not going to bother with the detailed pseudocode, since we've already done this, and it turns out the low level details of how we do this are not going to matter very much. So Prim's algorithm says we're going to be growing a minimum spanning tree from an arbitrary vertex, named \(r\) for root, one edge at a time. At each step, connect one more vertex to the tree with the minimum weight edge to the current tree.

\subsection{Kruskal's Algorithm}

In Kruskal's algorithm, at some point, we have a bunch of small disconnected trees. We look at the smallest edge we have left, and, if it connects two components, we merge those trees, otherwise, we throw it away (since it would create a cycle). So starting from \(T = \varnothing\), consider each edge in order of non-decreasing weight and add edges one by one until you have a spanning tree. We can tell this is the case if \(T\) has \(n - 1\) edges in it.

We can make this efficient by using the disjoint forest data structure to store the sets of connected vertices. It's not strictly necessary for what we're going to do here to do it with that level fo efficiency, since here all we care about is that there is some way to do this which takes polynomial time.

These are not the only two ways to do this. I'm going to give you one more. It doesn't have a name, nd it turns out that's for a good reason:

\subsection{The ``Reverse Delete'' algorithm}

Both Prim's algorithm and Kruskal's algorithm are based off the idea of starting with nothing and building up a spanning tree. This algorithm is based off the idea, bad for this problem but useful in general, of removing edges \textit{until} we have a spanning tree.

So we start with \(T = E\), and remove edges with the largest weight that would not disconnect \(T\) until we're left with \(n - 1\) edges.

So why doesn't this have a name? Prim's algorithm takes \(n\) iterations, and for each iteration I'm going to do some heap operations to keep track of which edge is the smallest one, so we know these can be done in time \(\log n\). So it takes roughly \(n\log n\), or really \(m\log n\) since it has to look at adjacent edges to update the priority queue. The running time for Kruskal's algorithm is similar.

What about this one? It turns out it runs in \(\Omega(mn)\). So while it's perfectly good for ``does it run in polynomial time,'' it doesn't run particularly fast...

Now let's talk about the correctness of Kruskal's algorithm at a high level, since it is closest, along with the reverse delete algorithm, to a pure greedy algorithm

\subsection{Correctness of Kruskal's Algorithm}

At a very high level, we
\begin{itemize}
  \item Sort edges by weight \(w(e_1) \leq ... \leq w(e_m)\)
  \item \(T = \varnothing\)
  \item For \(j \in \{1,...,m\}\), if the endpoints of \(e_j\) are \underline{not} connected in \(T\) add the edge to \(T\)
\end{itemize}
That's it. That's the whole algorithm. All of the difficulty of the algorithm comes from figuring out a good and clever way to figure out the answer of the question of whether the endpoints of \(e_j\) are connected in \(T\).

So now let's prove that for \(k = 0,1,...,m\), \(T_k\) can be extended to a minimum spanning tree \(T^*\), where \(T_k\) is the tree left after looking at edge \(k\). Since we're using sets, we can use the equation from last time
\begin{equation}
  T_k \subseteq T^* \subseteq T_k \cup \{e_{k + 1},...,e_m\}
\end{equation}
There is one small difference in the set up between this problem and the activity scheduling problem. With activity scheduling, what I could say is that after we look at the first \(k\) activities, we have a valid solution for just those first \(k\) edges. On the other hand, for this algorithm, each individual input may not be a valid input for the minimum spanning tree algorithm. So we can't say that at every step our partial solution is the best solution for the subproblem, so instead we're going to have to explicitly show that every partial solution can be completed to an actual solution.

So let's get started, by induction on \(k\).
\begin{itemize}

  \item Base case: \(k = 0\): \(T_0 = \varnothing\). This works, since any minimum spanning tree \(T^*\), being a set, must extend \(T_0\).

  \item Inductive hypothesis: suppose \(k \geq 0\) and \(T_k\) can be extended to some MST \(T^*\). There's only two possibilities for what \(T_{k + 1}\) could be: \(T_k\), or \(T_k \cup \{e_{k + 1}\}\). We'll do this next time.

\end{itemize}

\end{document}
