\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 18 - March 22 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}
\usepackage{xcolor}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ptrans}[0]{\xrightarrow{\Pt}}
\newcommand{\TODO}[1]{\textcolor{red}{\textbf{TODO:} #1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclareMathOperator{\Pt}{P}
\DeclareMathOperator{\NPt}{NP}
\DeclareMathOperator{\coNP}{coNP}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

So far, we have covered
\begin{itemize}

  \item Techniques for writing efficient algorithms, including greedy algorithms, dynamic programming, network flows and linear programming.

  \item Techniques for showing that problems \textit{cannot} be solved efficiently, that is, if \(\Pt \neq \NPt\).

  \item The traditional point of view that \(\Pt\) is ``easy'' and \(\NPt\)-complete is ``hard''

\end{itemize}
The problem with this point of view is that \(\NPt\)-completeness is based on worst-case analysis. What if inputs encountered in real life are rarely worst case? An even more important concern is that, just as \(\NPt\)-complete does not necessarily mean hard, \(\Pt\) does not necessarily mean easy either: for ``real-world'' input sizes (\(\geq 10^9\)), even \(\mc{O}(n^2)\) is too much!


This week, we'll be talking about the real world, and hence, approximations. Or rather,

\section{Approximation Algorithms}

Unless \(\Pt = \NPt\), it is impossible to solve \(\NPt\)-hard problems \textit{exactly} in polynomial time. In practice, we sometimes sacrifice on efficiency and use an exponential time algorithm hoping that inputs don't trigger the worst-case behaviour. This can be particularly useful on restricted families of inputs, as a problem that is hard in general may be much easier for certain inputs. For example, \(\proc{2Sat}\) and \(\proc{Independent-Set}\) on a tree are both easy. In fact, many graph problems are easier on trees or other restricted kinds of graph, though others, like 3-coloring, are still \(\NPt\)-complete.

Sometimes, instead of sacrificing efficiency, we instead sacrifice on \textit{exactness}. This is especially the case for optimization problems: instead of searching for the \textit{best} solution, we settle for one which is ``good enough.'' The scare quotes, of course, mean that we're going to elaborate on just what that means.

\subsection{What is Good Enough?}

For a minimization problem, we define \(\proc{Opt}(x)\) to be the minimum \textit{value} of any solution for an input \(x\). Suppose now that we have an approximation algorithm which generates solutions with an approximate value \(A(x)\). By definition, we have that, since \(\proc{Opt}(x)\) is minimum, \(\proc{Opt}(x) \leq A(x)\).

The \textbf{approximation ratio} of our algorithm is then a function \(r(n)\) such that
\begin{equation}
  \forall n \in \nats, \forall \text{ inputs } x \text{ of size } n, A(x) \leq r(n) \cdot \proc{Opt}(x)
\end{equation}
We can think of the approximation ratio as giving a bound on how much larger than the optimum our approximate value may be, i.e., it gives a guarantee that approximate values cannot be ``too'' large compared to optimum. Let's consider some examples:

\subsubsection{Vertex Cover}

Recall that a \textbf{vertex cover} \(C\) for a graph \(G = (V, E)\) is a set of vertices such that every edge in \(E\) has at least one endpoint in \(C\). \(\proc{Vertex-Cover}\) is the problem of finding a vertex cover \(C\) which minimizes \(|C|\).
Let's try a succession of approximations, trying to get \(C\) as low as we can:
\begin{enumerate}

  \item \TODO{copy from tutorial notes}

  \item Let's simply try repeatedly picking an edge from \(E\) and putting both endpoints in \(C\), removing all edges incident to either of the two endpoints from \(E\) afterwards until no edges remain. We have that
  \begin{equation}
    |C| \leq 2 \cdot \proc{Opt}(G)
  \end{equation}
  as all covers must include at least one endpoint for every edge picked in the first stage of the algorithm, implying \(\proc{Opt}(G) \geq |C|/2\). To show that the approximation ratio really is \(2\), all we need is the example of \(n\) disjoint edges, which will have all \(2n\) vertices placed into \(C\) even though only \(n\) are required.

  This gives a good example of how we can compute ratios in general without knowing \(\proc{Opt}\), especially for \(\NPt\)-hard problems like \(\proc{Vertex-Cover}\): using a lower bound, i.e. finding an easy to compute value \(L\) such that
  \begin{equation}
    \proc{Opt}(x) \leq L \land A \leq r \cdot L
  \end{equation}

  \item Finally, let's try using linear programming:
  \begin{itemize}

    \item Create a linear program from the input graph by assigning a variable \(x_i\) to each vertex \(v_i\), and setting the obvective function to be minimized to
    \begin{equation}
      \sum_{i = 1}^nx_i
    \end{equation}
    subject to the constraints
    \begin{equation}
      \forall i \in \{1,...,n\}, 0 \leq x_i \leq 1
    \end{equation}
    Note that, as an \textit{integer program}, this would be completely equivalent to \(\proc{Vertex-Cover}\), including the \(\NPt\)-hardness.

    \item Compute the optimal solution to the linear program. This is called \textbf{linear program relaxation}: allowing real values for variables.

    \item Create a cover \(C\) as follows: include \(v_i\) if and only if \(x_i \geq \frac{1}{2}\).

  \end{itemize}
  Note that this illustrates a way to approximate integer programming in general.
  Let's see how well this approximation works in the context of vertex covers. Consider a minimum vertex cover \(C'\), and let
  \begin{equation}
    \forall i \in \{1,...,n\}, x'_i = 1 \iff v_i \in C'
  \end{equation}
  Then, obviously we have that since \(x'_1,...,x'_n\) is a solution to the linear program given above, by the optimality of \(x_1,...,x_n\)
  \begin{equation}
    |C'| = \sum_ix'_i \geq \sum x_i
  \end{equation}
  On the other hand, let
  \begin{equation}
    \forall i \in \{1,...,n\}, \tilde x_i = 1 \iff v_i \in C \iff x_i \geq \frac{1}{2}
  \end{equation}
  Then for each \(i\),
  \begin{equation}
    \sum_i\tilde x_i \leq 2\sum_ix_i \leq 2\sum_ix'_i = 2|C'|
  \end{equation}
  giving the approximation ratio.

\end{enumerate}

\subsection{How Well Can We Approximate?}

Even though all \(\NPt\)-complete problems can in one way be considered ``equivalent'' to each other, the approximation raitos for their corresponding optimization problems are quite diverse.
\begin{itemize}

  \item \(\proc{Vertex-Cover}\) has a constant approximation ratio of 2, as we've seen

  \item \(\proc{Set-Cover}\), as in the textbook, has an approximation ratio of \(\log n\), i.e. not constant but limited

  \item \(\proc{Knapsack}\) has an approximation ratio of \(1 + \epsilon\) in time \(\mc{O}(n^3/\epsilon)\) for all \(\epsilon \in (0, 1]\).

  \item \(\proc{Travelling-Salesman}\) has \textit{no} finite ratio, unless \(\Pt = \NPt\)

\end{itemize}

\subsubsection{The Travelling Salesman Problem}

We begin with a statement of the problem:

\begin{itemize}

  \item Given: a graph \(G = (V, E)\) and a weigting \(w: E \to \reals^+_0\)

  \item Find: a ``tour'' (Hamiltonian cycle) with minimum total weight

\end{itemize}
This problem is \(\NPt\)-hard, having no polynomial time solution. Furthermore, it is \(\NPt\)-hard to approximate with a constant ratio: suppose we have an algorithm with an approximation ratio \(C(n)\). We can then use this algorithm to solve an \(\NPt\)-hard problem as follows:
\begin{itemize}

  \item Given an input \(G\) to the \(\proc{HamCycle}\) problem

  \item Construct an instance of \(\proc{Travelling-Salesman}\) \((G', w')\), with \(G' = (V, V \times V)\), and
  \begin{equation}
    w(e) = 1 \text{ if } e \in E \text{, else } w(e) = nC(n) + 1
  \end{equation}
  where \(n = |V|\).
  Any tour in \(G'\) using only edges with weight \(1\) has a total weight \(n\), whereas any tour in \(G'\) using at least one edge with weight \(nC(n) + 1\), i.e. not in \(E\), has total weight greater than \(nC(n)\). If \(G\) contains a Hamilton cycle, then \(G'\) contains a tour with total weight \(n\). Otherwise, all tours in \(G'\) have total weight \(\geq nC(n)\), as they must contain an edge not in \(G\).

  Now, all that is left to do is run the approximation algorithm on \(G'\). If it returns an anwer with total weight \(\leq nC(n)\), then \(G\) contains a Hamilton cycle, otherwise, it does not. Since this solves the Hamilton cycle problem, the approximation algorithm cannot run in polynomial time.

\end{itemize}

\end{document}
