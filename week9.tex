\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 4 - March 8 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}
\usepackage{xcolor}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ptrans}[0]{\xrightarrow{\Pt}}
\newcommand{\TODO}[1]{\textcolor{red}{\textbf{TODO:} #1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclareMathOperator{\Pt}{P}
\DeclareMathOperator{\NPt}{NP}
\DeclareMathOperator{\coNP}{coNP}
\DeclareMathOperator{\Assign}{Assign}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

\section{The Cook-Levin Theorem}

We begin by looking at some of the many variants of the satisfiability problem, \(\proc{Sat}\)
\begin{itemize}

  \item \(\proc{Circuit-Sat}\): given a (digital) circuit with a single bit as output, is there any input which will make that output \(1\)?

  \item \(\proc{Sat}\): given a propositional formula \(\phi\) in \(n\) Boolean variables \(a_1,...,a_n\) (written using the connectives \(\lnot, \land, \lor\) and \(\implies\)), is there an assignment of truth values to \(a_1,...,a_n\) such that \(\phi\) evaluates to true, i.e. is \(\phi\) satisfiable?

  \item \(\proc{CNF-Sat}\): given a propositional formula \(\phi\) in conjunctive normal form (product of sums), is \(\phi\) satisfiable?

  \item \(\proc{3Sat}\): given a propositional formula \(\phi\) in 3-CNF (conjunctive normal form where each clause is the disjunction of exactly three literals), is \(\phi\) satisfiable?

\end{itemize}
\begin{theorem}[Cook-Levin]
  \(\proc{Sat}\) is \(\NPt\)-complete
\end{theorem}
\begin{proof}
  \begin{itemize}

    \item \(\proc{Sat} \in \NPt\): we can easily verify an assignment of variables \(a_1,...,a_n\) satisfies \(\phi\) in time polynomial with respect to the length of \(\phi\) by simply evaluating \(\phi\) term by term and checking the result is \(\kw{True}\). The same reasoning applies to \(\proc{CNF-Sat}\) and \(\proc{3Sat}\), showing these are in \(\NPt\).

    \item \(\proc{Sat}\) is \(\NPt\)-hard (main idea): let \(D \in \NPt\) be arbitrary. Hence, by definition, we can find a verifier \(V(x, c)\) for \(D\) running in polynomial time. We can implement this verifier as a circuit with input gates representing the values of \(x\) and \(c\). Given an input \(x\), we can ``hard code'' the value of \(x\) into the inputs of the circuit to obtain a new circuit taking only \(c\) as an input in polynomial time. We can also convert this circuit's corresponding propositional formula can be transformed to CNF in polynomial time.

    Hence, since we can transform any \(\NPt\) problem to \(\proc{CNF-Sat}\) or \(\proc{Sat}\), we have shown that both are \(\NPt\)-hard. This leaves open the question of \(\proc{3Sat}\) for now, but we'll later show that \(\proc{3Sat}\) is \(\NPt\)-complete as well.

  \end{itemize}
\end{proof}

\section{\(\NPt\)-hardness}

The question remains: how do we show in general that a decision problem \(D\) is \(\NPt\)-hard. We don't want to, after all, re-prove the Cook-Levin theorem for every problem! The key to answering this lies in the fact that \(\ptrans\) is transitive: it is easy to show that
\begin{equation}
  D_1 \ptrans D_2 \land D_2 \ptrans D_3 \implies D_1 \ptrans D_3
\end{equation}
using function composition. Hence, we can show that \(D\) is \(\NPt\) hard merely by finding some \(\NPt\)-hard problem \(H\) such that \(D \ptrans H\), since
\begin{equation}
  D \ptrans H \implies \forall D' \in \NPt, D \ptrans H \ptrans D' \implies D \ \NPt \text{-hard}
\end{equation}

Let's now explore some examples of problems which are \(\NPt\)-complete:

\subsection{\(\proc{Subset-Sum}\)}

The problem \(\proc{Subset-Sum}\) is posed as follows:
\begin{itemize}
  \item Input: given a finite set of positive integers \(S \subseteq \mc{P}(\ints^+)\) and a positive integer \(t \in \ints^+\)
  \item Output: is there some subset \(S' \subseteq S\) such that \(\sum S' = t\)?
\end{itemize}

\begin{claim}
  \(\proc{Subset-Sum} \in \NPt\)
\end{claim}
\begin{proof}
  We can trivially construct a verifier running in polynomial time which just takes in a set of numbers \(S'\) and verifies it is a subset of \(S\) and sums up to \(t\).
\end{proof}

\begin{claim}
  \(\proc{Subset-Sum}\) is \(\NPt\)-hard
\end{claim}
\begin{proof}
  We want to show that \(\proc{Subset-Sum} \ptrans \proc{3Sat}\), the latter of which is \(\NPt\)-hard (we'll prove this later). To do so, assume we are given a formula \(\phi\) in 3-CNF (i.e. an input to \(\proc{3Sat}\)) such that
  \begin{equation}
    \phi = \bigwedge_{i = 1}^rC_i, C_i = (a_i \land b_i \land c_i)
  \end{equation}
  where each \(\{a_i, b_i, c_i\} \subseteq \{x_1, \lnot x_1, ..., x_s, \lnot x_s\}\). Construct the following set \(S = \{y_1,...,y_{2s + 2r}\}\) of numbers written in binary:
  \begin{itemize}

    \item For each \(j \in \{1,...,s\}\), define, where \(\ll_D\) is the right \textit{decimal} shift operator (i.e. \(x \ll_D n\) means prepend \(n\) zeroes to the \textit{decimal} representation of \(x\)), \(:_D\) is the \textit{decimal} concatenation operator (e.g. \(146 : 321 = 146321\))
    \begin{equation}
      y_{2j - 1} = (1 \ll_D (s - j)) :_D d_1 :_D ... :_D d_r, y_{2j} =  (1 \ll_D (s - j)) :_D d_1' :_D ... :_D d_r'
    \end{equation}
    where \(d_i = 1\) if \(x_j\) appears in \(C_i\) or \(0\) otherwise, and \(d_i' = 1\) if \(\lnot x_j\) appears in \(C_i\) or \(0\) otherwise.

    \item For each \(j \in \{1,...,r\}\), define
    \begin{equation}
      y_{2s + 2j - 1} = 1 \ll_D (r - j), y_{2s + 2j} = 2 \ll_D (r - j)
    \end{equation}

  \end{itemize}
  Now, define
  \begin{equation}
    t = t_1 :_D ... :_D t_{s + r}
  \end{equation}
  where \(t_1,...,t_s = 1\) and \(t_{s + 1},...,t_{s + r} = 4\). Clearly, this rather arcane construction can be performed in polynomial time. Here's an example:
  \begin{equation}
    \varphi = (x_1 \lor \lnot x_2 \lor \lnot x_4) \land (x_2 \lor \lnot x_3 \lor x_1) \land (\lnot x_3 \lor x_4 \lor \lnot x_2)
  \end{equation}
  yields \(t = 1111444\) and
  \begin{equation}
    \begin{array}{c ccc ccc c}
    S = \{& y_1 & = & 1000110,
    & y_2 & = & 1000000, \\
    & y_3 & = & 0100010,
    & y_4 & = & 0100101, \\
    & y_5 & = & 0010000,
    & y_6 & = & 0010011, \\
    & y_7 & = & 0001001,
    & y_8 & = & 0001100,\\
    & y_9 & = & 0000200,
    & y_{10} & = & 0000100, \\
    & y_{11} & = & 0000020,
    & y_{12} & = & 0000010, \\
    & y_{13} & = & 0000002,
    & y_{14} & = & 0000001 & \}
    \end{array}
  \end{equation}
  \(\phi\) is satisfiable if and only if we can set \(x_1,....,x_s\) in some way such that each \(C_1,...,C_r\) contains at least one true literal. Consider the subset \(S' = \{\text{ numbers that correspond to true literals }\}\). By construction,
  \begin{equation}
    \sum S' = s_1...s_{s + r}
  \end{equation}
  where \(s_1,...,s_{s} = 1\) and \(s_{s + 1},...,s_{s + r}\) are either 1, 2 or 3, since each clause contains at least one true literal (and at most 3). Hence we can add suitable numbers corresponding to each \(C_j\) (i.e. \(y_{2s + 2j}, y_{2s + 2j - 1}\)) to make the last \(r\) digits of the sum equal to 4, i.e. we can find a subset \(S'\) such that \(\sum S' = t\).

  On the other hand, assume there exists a subset \(S'\) such that \(\sum S' = t\). Then, since the first \(s\) digits of \(t\) are 1, it follows that \(S'\) must contain exactly one of each number corresponding to \(x_i, \lnot x_i\), i.e. \(y_{2i}, y_{2i - 1}\) for \(i \in \{1,...,s\}\). Hence, we can satisfy \(\phi\) by setting each variable accordingly: for each clause \(j\), the corresponding digit in \(t\) is \(4\) but each number corresponding to \(C_j\) (\(y_{2s + 2j}, y_{2s + 2j - 1})\) add up to 3.

  Hence, the selection of number in \(S'\) must include at least some literal with a 1 in the \((s + j)^{th}\) digit, i.e clause \(j\) has at least one true literal.

\end{proof}

Hence \(\proc{Subset-Sum}\) is \(\NPt\)-complete.

\subsection{\(\proc{Vertex-Cover}\)}

We can pose the problem \(\proc{Vertex-Cover}\) as follows:
\begin{itemize}
  \item Input: Given an undirected graph \(G = (V, E)\) and a positive integer \(k\)
  \item Output: Does \(G\) contain a vertex cover of size \(k\), i.e. can we find a subset \(C \subseteq V\) such that \(|C| = k\) and
  \begin{equation}
    \forall (u, v) \in E, u \in C \lor v \in C
  \end{equation}
\end{itemize}

\begin{claim}
  \(\proc{Vertex-Cover} \in \NPt\)
\end{claim}
\begin{proof}
  This was proved last week, but in brief, we can construct a verifier taking in \(G\), \(k\) and \(C\) and verifying that \(C\) contains \(k\) vertices and that, for each \((u, v) \in E\), \(u \in C \lor v \in C\). Obviously, this takes polynomial time.
\end{proof}

\begin{claim}
  \(\proc{Vertex-Cover}\) is \(\NPt\)-hard
\end{claim}
\begin{proof}
  Again, we want to show that \(\proc{3Sat} \ptrans \proc{Vertex-Cover}\). To do so, let \(\phi\) be a propositional formula in 3-CNF (i.e. an input to \(\proc{3Sat}\)) such that
  \begin{equation}
    \phi = \bigwedge_{i = 1}^rC_i, C_i = (a_i, b_i, c_i)
  \end{equation}
  where again \(\{a_i, b_i, c_i\} \subseteq \{x_1, \lnot x_1,..., x_s, \lnot x_s\}\). This time, however, instead of constructing a set of numbers, we'll be constructing a graph \(G = (V, E)\) as follows: define vertices
  \begin{equation}
    V = \{a_1,b_1,c_1,...,a_rb_r,c_r,x_1,\lnot x_1,...,x_s \lnot x_s\}
  \end{equation}
  And begin defining edges by defining
  \begin{equation}
    E_{T_i} = \{(a_i, b_i), (a_i, c_i), (a_i, d_i)\}, E_T = \bigcup_{i = 1}^rE_{T_i}
  \end{equation}
  \begin{equation}
    E_N = \{(x_i, \lnot x_i) : i \in \{1,...,s\}\}
  \end{equation}
  \begin{equation}
    E_{C_i} = \{(a_i, \Assign(a_i)), (b_i, \Assign(b_i)), (c_i, \Assign(c_i))\}, E_C = \bigcup_{i = 1}^rE_C
  \end{equation}
  where \(\Assign(a_i)\) is the first element in \(C_i\), \(\Assign(b_i)\) is the second and \(\Assign(c_i)\) the third. We can now define \(E = E_T \cup E_N \cup E_C\). Clearly, we can perform this construction in polynomial time (since we only need to scan \(\phi\) once). An example would be, for
  \begin{equation}
    \varphi = (x_1 \lor \lnot x_2 \lor \lnot x_4) \land (x_2 \lor \lnot x_3 \lor x_1) \land (\lnot x_3 \lor x_4 \lor \lnot x_2)
  \end{equation}
  generating the graph
  \begin{equation}
    V = \{a_1, b_1, c_1, a_2, b_2, c_2, a_3, b_3, c_3, x_1, \lnot x_1, x_2, \lnot x_2, x_3, \lnot x_3, x_4, \lnot x_4\}
  \end{equation}
  \begin{equation}
    E_{C_1} = \{(a_1, x_1), (b_1, \lnot x_2), (c_1, \lnot x_4)\}, E_{C_2} = \{(a_2, x_2), (b_2, \lnot x_3), (c_2, x_1)\}, E_{C_3} = \{(a_3, x_3), (b_3, x_4), (c_3, \lnot x_2)\}
  \end{equation}
  with \(E_T, E_N\) trivially generated and \(E\) as above.
  Define \(k = s + 2r\) (in the above case \(k = 4 + 2 \cdot 3 = 10\)).

  \begin{itemize}

    \item If \(\phi\) is satisfiable, we can find an assignment of truth values that make at least one literal in each clause true. We can then construct a cover \(C\) as follows:
    \begin{itemize}
      \item For each \(x_i\), \(C\) contains \(x_i\) if \(x_i\) is true under the truth assignment or \(\lnot x_i\) otherwise
      \item For each \(C_i\), \(C\) contains every literal (\(a_i, b_i\) or \(c_i\)) except \textit{one} which is true (if more than one literal in the clause is true, pick one to be excluded). Hence, that's 2 literals (vertices) per clause.
    \end{itemize}
    Hence, \(C\) is of size \(s\) (one vertex per variable) \(+ 2r\) (two vertices per clause), and:
    \begin{itemize}
      \item All edges \((x_i, \lnot x_i) \in E_N\) are covered, since each \(x_i\) has a truth assignment and hence one of the two values chosen
      \item All edges in clause triangles \(E_{T_i}\) are covered, since 2 of the 3 literals \(\{a_i, b_i, c_i\}\) in the clause are included in \(C\)
      \item All edges in \(E_{C_i}\) are covered since two of the \(\{a_i, b_i, c_i\}\) will be chosen, and the other value, since it is true, will be the appropriate \(x_j\) or \(\lnot x_j\).
    \end{itemize}

    \item If \(G\) contains a cover \(C\) such that \(|S| = k = s + 2r\), due to the edges \(E_N\), the cover must contain at least one of \(x_i\) or \(\lnot x_i\). Due to the triangles \(E_{T_i}\), it must contain at least two of \(\{a_i, b_i, c_i\}\). So the only way for \(C\) to have size \(s + 2r\) is for it to contain exactly one of \(x_i\) or \(\lnot x_i\) and exactly two of \(\{a_i, b_i, c_i\}\).

    Since \(C\) covers \textit{all} edges but has only two vertices per triangle, it follows that, for each \(y \in \{a_i, b_i, c_i\}\) \textit{not} in \(C\), it's corresponding \(x_j\) or \(\lnot x_j\) (given by \(E_{C_i}\)) must be in the cover. Hence, setting literals according to whether \(x_i\) (set \kw{True}) or \(\lnot x_i\) (set \kw{False}) is in \(C\) satisfies formula \(\phi\), since at least one literal will be true in each clause, as at least one edge from ``variables'' \(\{a_i, b_i, c_i\}\) to
    clauses \(\{x_j, \lnot x_j\}\) must be covered in \(C\).

  \end{itemize}

\end{proof}

Hence \(\proc{Vertex-Cover}\) is \(\NPt\)-complete.

\subsection{\(\proc{3Sat}\)}

\(\proc{3Sat}\) is trivially in \(\NPt\) as it is a special case of \(\proc{Sat}\), which is in \(\NPt\). So we only have to prove that
\begin{claim}
  \(\proc{3Sat}\) is \(\NPt\)-hard
\end{claim}
\begin{proof}
  Here, we attempt to show that \(\proc{CNF-Sat} \ptrans \proc{3Sat}\).
  \TODO{Copy the rest}
\end{proof}

Be careful with the directions you try to prove. It is trivial that \(\proc{3Sat} \ptrans \proc{CNF-Sat}\) since the former is just a special case of the latter (so we can use the identity mapping on inputs), but as we saw above the reverse direction is what we needed for \(\NPt\)-completeness and is nontrivial.

\subsection{\(\proc{Clique}\)}

We can state the \(\proc{Clique}\) problem as follows:
\begin{itemize}

  \item Input: given an undirected graph \(G = (V, E)\) and a positive integer \(k\)

  \item Output: does \(G\) contain a \(k\)-clique, i.e. a subset of vertices \(C \subseteq V\) such that \(|C| = k\) and all edges between vertices in \(C\) are present in the graph, that is,
  \begin{equation}
    \forall u, v \in C, (u, v) \in E
  \end{equation}

\end{itemize}
\begin{claim}
  \(\proc{Clique} \in \NPt\)
\end{claim}
\begin{proof}
  Consider the verifier which takes in \(G, C, k\), and, in polynomial time, constructs all pairs \((u, v)\) of elements in \(C\) and checks if they are in \(E\), and furthermore verifies that \(|C| = k\). If this condition holds and all pairs are in \(E\), we return \(\kw{True}\), otherwise we return \(\kw{False}\).
\end{proof}

\subsection{\(\proc{SubgraphIsomorphism}\)}

We can state the problem \(\proc{SubgraphIsomorphism}\) as follows:
\begin{itemize}

  \item Input: Given two graphs \(G = (V, E)\) and \(H = (V', E')\)

  \item Output: Is \(G\) a subgraph of \(H\), i.e. can we map every vertex in \(G\) to some vertex in \(H\) such that, on those vertices, \(H\) contains the same edges as \(G\)?

\end{itemize}

\begin{claim}
  \(\proc{SubgraphIsomorphism} \in \NPt\)
\end{claim}
\begin{proof}
  We define a verification procedure \(P\) taking as parameters \(G, H\) and some mapping \(f: V \to V'\) represented as a set \(S\) of pairs \((g, h)\), where \(g \in V\) is mapped to \(h \in V'\) as follows:
  \begin{itemize}
    \item If \(|L| \neq |V|\) or \(S\) is not a function (\(\exists g \in V, \exists h, h' \in V', (g, h) \in S \land (g, h') \in S\)), return \(\kw{False}\)
    \item If \(S\) taken as a function is not injective (\(\exists g, g' \in V, \exists h \in V', (g, h) \in S \land (g, h') \in S\))
    \item For each \(u, v \in V\), check \((u, v) \in E \iff (f(u), f(v)) \in E'\)
  \end{itemize}
  Both these steps can be completed in polynomial time, since evaluating \(f(u)\) takes at most linear time for each \(u\).
  \begin{itemize}

    \item If \(G\) is a subgraph of \(H\), then we can find a mapping \(f\) from every vertex in \(G\) to the vertices in \(H\) such that the image of \(G\) in \(H\) has the same edges as \(G\). Let \(S\) be the set corresponding to such a mapping. By definition, it is an injective function and has size \(|V|\) (since it maps each vertex of \(G\) to a vertex in \(H\)), and \(\forall u, v \in V, (u, v) \in E \iff (f(u), f(v)) \in E\). Hence, \(P(G, H, S) = \kw{True}\)

    \item If \(G\) is \textit{not} a subgraph of \(H\), then either
    \begin{itemize}

      \item \(|V'| < |V|\), in which case we cannot construct an injective mapping \(f\), and hence any set \(S\) passed to the verifier will not be injective, implying every certificate returns \(\kw{False}\)

      \item \(|V'| \geq |V|\), in which case for every injective mapping \(f\),
      \begin{equation}
        \exists u, v \in V, [(u, v) \in E] \neq [(f(u), f(v)) \in E]
      \end{equation}
      implying every set \(S\) is either not injective or fails the third part of the test, implying every certificate again returns \(\kw{False}\)

    \end{itemize}
    Hence, it follows that \(\exists S, P(G, H, S) = \kw{True} \iff \) \(G\) is a subgraph of \(H\), i.e. \(P\) is a valid verifier for \(\proc{SubgraphIsomorphism}\). Since it runs in polynomial time, the claim follows.

  \end{itemize}
\end{proof}
\begin{claim}
  \(\proc{SubgraphIsomorphism}\) is \(\NPt\)-hard
\end{claim}
\begin{proof}
  We want to show that \(\proc{Clique} \ptrans \proc{SubgraphIsomorphism}\).

  Let \(H\) be a graph and \(k\) be a positive integer, i.e. \((H, k)\) be an input to the problem \(\proc{Clique}\).
  \begin{itemize}
    \item If \(k^2 > |E|\), \(H\) cannot contain a \(k\)-clique, so we only need to construct an arbitrary input \(G', H'\) for which \(\proc{SubgraphIsomorphism}(G', H')\) is false, for example, \(G' = H, H' = (\varnothing, \varnothing)\).
    \item Construct a \(k\)-clique \(G\) in polynomial time by creating a list of \(k\) vertices and \(k^2\) edges connecting each vertex (this is polynomial time since \(k^2 \leq |E|\)). Then \(G' = G\) is a subgraph of \(H' = H\) if and only if \(H\) has a \(k\)-clique
  \end{itemize}
  Hence, in either case
  \begin{equation}
    \proc{Clique}(H, k) \iff \proc{SubgraphIsomorphism}(G', H')
  \end{equation}
  as desired.
\end{proof}

\subsection{\(\proc{MaxSat}\)}

We can state the \(\proc{MaxSat}\) problem as follows:
\begin{itemize}

  \item Input: Given a CNF formula \(\phi = \land_{i = 1}^rC_i\) and a positive integer \(k\)

  \item Output: is there an assignment of truth values that satisfies at least \(k\) clauses of \(\phi\)?

\end{itemize}

\begin{claim}
  \(\proc{MaxSat} \in \NPt\)
\end{claim}
\begin{proof}
  We construct a verifier \(V\) taking as inputs \(\phi\), an assignment of truth values \(a\) to \(\phi\), \(k\) and a subset of \(S\) of \(\{1,...,n\}\) of size \(k\). We return true iff
  \begin{equation}
    \forall s \in S, C_s(a) = \kw{True}
  \end{equation}
  Obviously, this can be done in polynomial time. If an assignment \(a\) of truth values that satisfies at least \(k\) clauses of \(\phi\) exists, then  \(V(\phi, a, k, S) = \kw{True}\) where \(S\) is a subset of size \(k\) of the set \(S'\) of clauses satisfied by \(\phi\). On the other hand, if an assignment \(a\) and a subset \(S\) exist, then \(a\) satisfies the \(\proc{MaxSat}\) so \(\phi, k\) is a yes-instance.
\end{proof}
\begin{claim}
  \(\proc{MaxSat}\) is \(\NPt\)-hard
\end{claim}
\begin{proof}
  We trivially have that \(\proc{Sat} \ptrans \proc{MaxSat}\), as \(\phi\) is a yes-instance of \(\proc{Sat}\) if and only if \(\phi, r\) is a yes-instance of \(\proc{MaxSat}\), where \(r\) is the number of clauses in \(\phi\).
\end{proof}

\subsection{\(\proc{SparseSubgraph}\)}

We can state the \(\proc{SparseSubgraph}\) problem as follows:
\begin{itemize}

  \item Input: Given a graph \(G\) and positive integers \(a, b \in \ints^+\)

  \item Output: is there a subset of \(a\) vertices of \(G\) with no more than \(b\) edges between them

\end{itemize}
\begin{claim}
  \(\proc{SparseSubgraph} \in \NPt\)
\end{claim}
\begin{proof}
  \TODO{solve}
\end{proof}

\begin{claim}
  \(\proc{SparseSubgraph}\) is \(\NPt\)-hard
\end{claim}
\begin{proof}
  \TODO{solve}
\end{proof}

\subsection{\(\proc{SetCover}\)}

We can state the \(\proc{SetCover}\) problem as follows:
\begin{itemize}

  \item Input: Given a set \(S\), subsets \(S_1,...,S_m \subseteq S\) and a positive integer \(k\)

  \item Output: is there a collection of \(k\) subsets whose union is equal to \(S\)

\end{itemize}
\begin{claim}
  \(\proc{SetCover} \in \NPt\)
\end{claim}
\begin{proof}
  \TODO{solve}
\end{proof}

\begin{claim}
  \(\proc{SetCover}\) is \(\NPt\)-hard
\end{claim}
\begin{proof}
  \TODO{solve}
\end{proof}

\subsection{Reductions Revisited}

For some more examples of reductions, let's construct polynomial time reductions between \(\proc{VertexCover}\), \(\proc{IndependentSet}\), \(\proc{Clique}\).
\begin{itemize}

  \item \(\proc{VertexCover} \ptrans \proc{IndependentSet}\)

  \TODO{solve}

  \item \(\proc{VertexCover} \ptrans \proc{Clique}\)

  \TODO{solve}

  \item \(\proc{IndependentSet} \ptrans \proc{VertexCover}\)

  \TODO{solve}

  \item \(\proc{IndependentSet} \ptrans \proc{Clique}\)

  \TODO{solve}

  \item \(\proc{Clique} \ptrans \proc{VertexCover}\)

  \TODO{solve}

  \item \(\proc{Clique} \ptrans \proc{IndependentSet}\)

  \TODO{solve}

\end{itemize}

\end{document}
