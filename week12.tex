\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{March 25 - March 29 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}
\usepackage{xcolor}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ptrans}[0]{\xrightarrow{\Pt}}
\newcommand{\TODO}[1]{\textcolor{red}{\textbf{TODO:} #1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclareMathOperator{\Pt}{P}
\DeclareMathOperator{\NPt}{NP}
\DeclareMathOperator{\coNP}{coNP}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

\section{The Travelling Salesman Problem, Continued}

We now consider the special case of the Travelling Salesman Problem with the triangle inequality, i.e.
\begin{equation}
  \forall x, y, z \in V, w(x, y) \leq w(x, z) + w(z, y)
\end{equation}
It turns out that this has a 2-approximation algorithm:
\begin{enumerate}

  \item Construct a minimum spanning tree \(T\) of \(G\)

  \item Construct a Eulerian tour of \(G\) by travelling along each edge of \(T\) once in each direction, starting from an arbitrary leadt \(f\) in \(T\).

  \item Construct a tour \(C\) of \(G\) from the Eulerian tour by:
  \begin{enumerate}

    \item Start with the current node \(c = f\), marking it as ``visited'' (with all other nodes ``unvisited'')

    \item Until \(n = f\), let \(n\) be the next node in the Eulerian tour. If \(n\) is unvisited, add \((c, n)\) to the cycle \(C\), then let \(c = n\) and mark it as visited. Otherwise, do nothing

    \item Add the edge \((c, f)\) to \(C\). This closes the tour as \(c\) is the last node visited.

  \end{enumerate}

\end{enumerate}
To find the approximation ratio of this algorithm, consider any optimum tour \(C^*\), and let \(e \in C^*\) be the edge with maximum weight. Then \(C^* \setminus \{e\}\) is a Hamilton path in \(G\), which is a special kind of spanning tree. Hence, since \(T\) is an MST, it follows that
\begin{equation}
  cost(C^*) \geq cost(C^*\setminus\{e\}) \geq cost(T)
\end{equation}
Consider now the tour \(C\) found by the algorithm. We have that, since \(C\) is obtained from an Eulerian cycle based off \(T\) (which costs exactly \(2 \cdot cost(T)\)) through a series of operations which can only reduce cost (by the Triangle Inequality),
\begin{equation}
  cost(C) \leq 2 \cdot cost(T)
\end{equation}
Hence, putting these two facts together
\begin{equation}
  cost(C) \leq 2 \cdot cost(T) \leq 2 \cdot cost(C^*)
\end{equation}

A similar idea using a perfect matching instead of an MST yields an algorithm with an approximation ratio of \(\frac{3}{2}\), but the algorithm and proof of the associated approximation ratio are more complicate.

\section{Knapsack}

We begin, as usual, with a statement of the problem:
\begin{itemize}

  \item Given: a weight limit \(W\), and \(n\) items as pairs \((v_i, w_i) \in \nats^2\) (where \(v_i\) is the ``value'' and \(w_i\) is the ``weight'' of the \(i^{th}\) item)

  \item Find a selection of items \(S \subseteq \{1,...,n\}\) such that the total weight of the selected items does not exceed \(W\), i.e.
  \begin{equation}
    \sum_{s \in S}w_s \leq W
  \end{equation}
  maximizing the total value
  \begin{equation}
    \sum_{s \in S}v_s
  \end{equation}

\end{itemize}
This problem is \(\NPt\)-hard but can be solved using dynamic programming in time \(\Theta(nV)\), where
\begin{equation}
  V = \sum_{i = 1}^nv_i
\end{equation}
with an \(n \times V\) array \(W[i][j]\) to store the minimum weight required to achieve a total value of at least \(j\) using items from \(\{1,...,i\}\) for \(0 \leq i \leq n, 0 \leq j \leq V\). If the values are large compared to \(n\), then \(\Theta(nV)\) is not polynomial.

The trick for the approximation is to used scaled down values. For example, if we have items with values
\begin{equation}
  v_1 = 12,356,444, v_2 = 49,495,858, v_3 = 78,498,675
\end{equation}
then solve the problem with values scaled down to \(123, 494, 784\). The loss of precision may yield a solution which is not optimal for the original input, but it should be close.

More generally, for a constant \(\epsilon\), use dynamic programming to find an optimum solution \(S'\) for input \((w_i, v_i')\) where \(v_i' = \floor{\frac{n}{M\epsilon}v_i}\) for \(M = \max(v_1,...,v_n)\), and output \(S'\).

This algorithm runs in time
\begin{equation}
  \mc{O}(nV') = \mc{O}(n*n*n/\epsilon) = \mc{O}(n^3/\epsilon)
\end{equation}
To compute the approximation ratio, we proceed as follows: we have that
\begin{equation}
  v_i' = \floor{\frac{n}{M\epsilon}v_i} \leq v_i \cdot \frac{n}{M\epsilon}
\end{equation}
implies that
\begin{equation}
  \begin{split}
  \sum_{i \in S'}v_i \geq \sum_{i \in S'}v_i' \cdot \frac{M\epsilon}{n}v_i  \geq \frac{M\epsilon}{n}\sum_{i \in S^*}v_i' = \frac{M\epsilon}{n}\sum_{i \in S^*}\floor{\frac{v_in}{M\epsilon}}
  \\
  \\ \geq \frac{Me}{n}\sum_{i \in S^*}\left(\frac{v_in}{M\epsilon} - 1\right)
   = \cancel{\frac{M\epsilon}{n}\frac{n}{M\epsilon}}\sum_{i \in S^*}v_i - \frac{M\epsilon}{m}\sum_{i \in S^*}1
  \\
  \\ = \proc{Opt}(x) - \frac{M\epsilon}{n}|S^*| \geq \proc{Opt}(x) - \frac{M\epsilon}{n}n \geq (1 - \epsilon)\proc{Opt}(x)
  \end{split}
\end{equation}
since \(\proc{Opt}(x) \geq M\) and \(|S^*| \leq n\),
where \(S^*\) is an optimum solution for the original input as \(S'\) is optimum for the scaled down input.

Note that since this is a maximization problem, the approximation ratio is defined as a real valued function \(r(n)\) such that
\begin{equation}
  r(n) \cdot A(x) \geq \proc{Opt}(x) \iff A(x) \geq \frac{\proc{Opt}(x)}{r(n)}
\end{equation}
Hence, in this case we have
\begin{equation}
  r(n) \leq \frac{1}{1 - \epsilon}
\end{equation}

\section{Randomization}

Randomized algorithms are a very important tool in computer science. While diverse, they can be broadly classified as:
\begin{itemize}

  \item ``Las Vegas'' algorithms: the solution is guaranteed to be correct, however the runtime depends on random choices. Examples include randomized quicksort.

  \item ``Monte Carlo'' algorithms: the runtime is determinsitic, but the answer is random.

\end{itemize}
Algorithms where both the runtime and output are random are not used in practice.

\subsection{Miller-Rabin Primality Testing}

The problem of primality testing is easily stated: given \(m \in \ints^+\), is \(m\) prime? It is a recent research result that there exists an \(\mc{O}(n^3)\) determinsitic algorithm for this problem, where \(n = \log_2m\). However, this algorithm is too slow in practice for large \(n\), whereas the Miller-Rabin algorithm runs in \(\mc{O}(n)\). It is a Monte-Carlo algorithm with an error probability of less than \(0.5\).

If the algorithm returns ``composite'', then \(m\) is composite, else, if the algorithm returns ``pseudoprime'', then \(m\) is \textit{probably} prime. More specifically, if \(m\) is composite, then the probability that the algorithm returns ``pseudoprime'' is less than \(\frac{1}{2}\). If we run the algorithm \(k\) times, the runtime increases to \(\mc{O}(k\log m)\) but the probability of error decreases to \(2^{-k}\).

For most applications where prime numbers are needed, pseudoprime numbers work just as well, even if the pseudoprime number is actually composite.

\section{Backtracking, branch and bound}

The idea here is to brute-force with a cutoff: while constructing possible solutions, we rule out any partial solution we cannot complete. For optimization problems, we can use an easy to compute approximation to the optimal value to rule out bad possiblities early. This is called ``branch-and-bound''.

This is commonly used in \(\proc{SAT}\) solvers for constraint satisfaction problems.

\section{Local Search}

For local search, we define a notion of ``local change'' for a problem. For example, we could replace disjoint edges \((u_1, v_1)\) and \((u_2, v_2)\) with \((u_1, v_2)\) and \((u_2, v_1)\) in a travelling salesman problem circuit. Then, starting from some initial candidate, we repeatedly make local changes as long as it improves the value of our candidate.

One issue with this is that the runtime may not be polynomial. We can counter this just by stopping the process after a certain time: the solution will still be better than the initial one, even if it is not optimal. Another problem is that of finding locally optimal solutions that are not globally optimal. This can be tackled by running the algorithm multiple times from various starting points, and/or by using a ``simulated annealing'' technique which allows non-improving changes with some probability that decreases with runtime.

Evolutionary algorithms, also known as genetic programming, are types of local search algorithms.

\end{document}
