\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 9 2019}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}

\begin{document}

\maketitle

\section{Activity Scheduling}

Last time, we began to consider the problem of activity scheduling: given a set of activities
\begin{equation}
  \mc{A} = \{A_1 = (s_1, f_1),...,A_n = (s_n, f_n)\}
\end{equation}
with given start and finished times, output a subset \(S \subseteq \mc{A}\) with no overlapping activities, maximizing \(|S|\).

\subsection{Algorithm A: Brute Force}

Let's consider the most simple solution: brute force. Even if it takes \(\mc{O}(1)\) time to check if a subset \(S\) has no overlaps, we still have \(2^n\) subsets, so overall we have lower bound on runtime \(\Omega(2^n)\), implying the algorithm cannot be computed in polynomial time.

\subsection{Algorithm B: Greedy by Start Time}

Let's try a greedy approach instead:
\begin{codebox}
\Procname{\(\proc{Greedy-By-Start}(\mc{A})\)}
  \li Sort activites by start time (\(s_1 \leq ... \leq s_n\))
  \li \(S \gets \varnothing\)
  \li \(f \gets 0\)
  \li \For \(i \in 1,...,n\) \If \(s_i \geq f\) \Then
  \li   \(S \gets S \cup \{A_i\}\)
  \li   \(f \gets f_i\) \End
  \li \Return \(S\)
\end{codebox}
The runtime is \(\mc{O}(n\log n)\) (for sorting), which is clearly polynomial? But is it correct? Nope: this goes wrong, for example, if we have a very long activity starting just before a sequence of many small activites.

What I'm trying to illustrate here is the process of trying things, slowly converging towards a working solution. Let's modify our approach:

\subsection{Algorithm C: Greedy by Duration}

Let's try sorting by \(f_i - s_i\) instead. Now, we have to be a bit more clever in checking for overlap, but we can just implement this naively and check every other element, which is still \(\mc{O}(n^2)\) and hence polynomial (which is mostly what we care about in this course).

Does this work? No: let's see where it breaks: imagine an input containing very short activites covering the edges of mutiple long activites, with no other activites. Clearly, we'll pick the short activites first, and then forgo the (twice as numerous) long activities.

The problem I chose is fairly simple, so some of the solutions here look like obvious decoys. But as soon as problems start to get more complicated, these decoys start to get less obvious.

Why did these previous approaches failed: they used activites which might have overlapped with many other activites, even if they started early or were short. But what was the \textit{real} intuition behind picking shorter activities first? We wanted to pick the activities which overlapped with the fewest other activities. So let's do just that.

\subsection{Algorithm D: Greedy by Overlap Count}
Let's try sorting by the count of activities with which an activity overlaps. This deals correctly with the pathological cases described to break Algorithms B and D, but does it work?

No: consider, for example, two long activities, and then \textit{three} short activities, all at the same time, covering the edge between the two long activities. This still works, but with a little fiddling, we can make a little trick: consider four long activities, non overlapping, and 3 short activities on the edges between the first and second and the third and fourth respectively, and one short activity between the second and third.

The short activity between the second and third long activities overlaps exactly 2 activities, while all others overlap 3 or 4, so it will be picked first. But it rules out the second and third long activities, allowing us only to pick 2 other activities, whereas the best choice would be to pick the 4 long activities.

So let's try something else:
\subsection{Algorithm E: Greedy by End Time}
The way to think of greedy by end time is going to be easy, because the algorithm is going to be exactly the same as Algorithm B, changing only the sorting criterion to be by end time, giving an \(\mc{O}(n\log n)\) algorithm.

In pseudocode, that's
\begin{codebox}
\Procname{\(\proc{Greedy-By-End}(\mc{A})\)}
  \li Sort activites by start time (\(f_1 \leq ... \leq f_n\))
  \li \(S \gets \varnothing\)
  \li \(f \gets 0\)
  \li \For \(i \in 1,...,n\) \If \(s_i \geq f\) \Then
  \li   \(S \gets S \cup \{A_i\}\)
  \li   \(f \gets f_i\) \End
  \li \Return \(S\)
\end{codebox}

Why is it a good idea to sort by end time? Because while picking activities, we want to pick activities that ``free the resource'' as quickly as possible, to let another activity start as soon as possible.

Now, you should be feeling a little bit confused. I went through all these iterations to show you a whole bunch of algorithms that look as reasonable as this. So how do we know which one is true? That's always the thing with greedy algorithms: they're almost always pretty much trivial at the end. The difficult part is: \textit{why} do they work, and why not the others? And how do we convince ourselves that this is correct?

Well, we are in computer science. When we're trying to determine if something is true or false, we like to do proofs. So let's do a proof!

What I'm going to show you is a general proof technique for trying to prove the correctness of greedy algorithms. There's lots of different techniques we could try to prove this problem correct: we could do some kind of proof by contradiction, or something else. But there's one kind of natural proof structure that fits the structure of this algorithm: if we can do a sort of proof by induction, checking that our result is correct at every step, then we're done. What's nice about this structure is it will work with any greedy algorithm, even if it sometimes requires a bit more work than another proof structure would've.

So I'm going to go back to my algorithm, in pseudocode, and consider the sequence of subsets created by the \kw{for} loop. So consider \(S_0, S_1,..., S_n\) - partial solutions generated by the algorithm, where \(S_i\) is the value of \(S\) at the end of iteration \(i\).

In this case, what we could do is we could say that, can I prove that, at any point in the execution of this algorithm, my algorithm will have, after looking at the first \(k\) activities, maybe \(S_k\) is the best solution for the first \(k\) activities. Maybe I'm creating a partial solution, but I'm using partial input as well. If I can prove that that's true for every iteration by induction, then I'd be done.

This is a natural thing to consider, and a perfectly valid proof of the correctness of this particular algorithm, but if you try to use this same idea for the correctness of some other greedy algorithms, it may not work, because you might not be able to carve out a complete partial input to go with your partial solution. So we're going to try a different approach, for which this is the rough intuition, even though it might not yet make complete sense yet.

So we selected some activities from the first \(k\) in a way that we think is going to lead to the solution for the first \(k\). What we're going to prove is essentially the following statement: if I look at these, from the point \(k\), then there is always some optimal solution, an actual, best solution to the problem \(S^*\), that contains everything I've included so far, and contains no activities that my algorithm has already rejected (among the first \(k\)).

If we can prove this, then by the time I'm done, we know that \(S_n = S^*\), since there is an optimal soluton which contains exactly everything in my solution, no more no less, and hence my solution must be optimal. So let's start:

\begin{definition}
  \(S_k\) is \underline{promising} if there is some optimum solution \(S^*\) that \underline{extends} \(S_k\), i.e., such that
  \begin{equation}
    S_k \subseteq S^* \subseteq S_k \cup \{A_{k + 1},...,A_n\}
  \end{equation}
\end{definition}
Note here I can write the definition of a solution extending the other just with subset notation, but we might have to define what it means for one solution to extend another if we're not using sets.

Now, an easy mistake to make the first time you look at this is that I'm looking for this one particular optimal solution. That's not quite right, because there might be multiple solutions which are all optimal but not compatible with each other.

Now, given that \(S_n\) is promising, then there is some optimum solution \(S^*\) such that
\begin{equation}
  S_n \subseteq S^* \subseteq S_n \cup \varnothing \implies S_n = S^*
\end{equation}
i.e. \(S_n\) is optimum.

So, we know this is going to give us the conclusion we want: that the algorithm generates an optimum conclusion. All we have to do now is prove this, which we will using a standard induction on the number of iterations in the loop, which we will do on Friday.

\end{document}
