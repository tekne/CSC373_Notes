# CSC373 Notes

My notes from CSC373: Algorithm Design, Analysis & Complexity (LEC0101: Professor Francois Pitt) in LaTeX. Warning: may be incomplete/incorrect!

Notes marked "Based off notes by Professor Francois Pitt" were directly adapted, with significant borrowing (e.g. of phrases/sentences/formalism), from typed notes by Professor Francois Pitt on the course website, which he has generously provided me permission to do.
