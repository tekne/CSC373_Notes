\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{February 04-08 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}

\begin{document}

\maketitle

\section{Network Flow}

\begin{definition}
  A \textbf{network} is a directed graph \(N = (V, E)\) with
  \begin{itemize}
    \item A single ``source'' \(s \in V\) with no incoming edge
    \item A single ``sink'' \(t \in V\) with no outgoing edge
    \item A nonnegative integer capacity \(c(e)\) for each edge in \(E\)
  \end{itemize}
\end{definition}
Networks can be used to model computer networks (with, for example, capacity as bandwidth), electrical networks, etc.
\begin{definition}
  The \textbf{network flow problem} is defined as follows: assign a flow \(f(e) \in \reals\) to each edge \(e\) such that we have maximum flow ``in the network'' (to be defined), subject to the following constraints
  \begin{itemize}
    \item \textbf{Capacity}: for each edge \(e\), \(0 \leq f(e) \leq c(e)\), i.e. ``flow does not exceed capacity''
    \item \textbf{Conservation}: for each vertex \(v \notin \{s, t\}\), \(f^{in}(v) = f^{out}(v)\), where \(f^{in}(v)\) denotes the total flow into \(v\) given by
    \begin{equation}
      f^{in}(v) = \sum_{(u, v) \in E}f(u, v)
    \end{equation}
    and \(f^{out}(v)\) denotes the total flow into \(v\) given by
    \begin{equation}
      f^{out}(v) = \sum_{(v, u) \in E}f(v, u)
    \end{equation}
  \end{itemize}
  The \textbf{total flow} in the network is denoted \(|f|\) and defined as \(|f| = f^{out}(s)\). We will later prove that, by conservation \(|f| = f^{in}(t)\).
\end{definition}
Let's get started, as always, with brute force. In this case, we have a startling
\begin{equation}
  \Omega\left(\prod_{e \in E}c(e) + 1\right)
\end{equation}
possibilities, assuming we try to assign a nonnegative flow to each edge \(e\). Much worse than a mere exponential?

Let's think about how we can improve on this dismal situation. One idea is a local search strategy: start with an initial assignment of flow which is guaranteed to be correct, even if it isn't maximal, and then try to make incremental improvements, stopping when no improvement is possible. This leads to the Ford-Fulkerson algorithm: start woith any valid flow \(f\) (e.g. \(f(e) = 0\) everywhere) and, while there is an ``augmenting path'' \(P\), augment \(f\) using \(P\). Then, output \(f\).

Two problems: what's an ``augmenting path'' and how do we ``augment'' a flow?
\begin{itemize}

  \item Intuition: since all flow ``starts'' at \(s\) and ``ends'' at \(t\), find paths from \(s\) to \(t\) along which flow can be increased. Adding flow along a path preserves conservation. But, how do we find paths?

  \item One idea is to find a path \(P\) from \(s\) to \(t\) where \(f(e) < c(e)\) for each \(e\). We define the ``residual capacity'' for edges as follows:
  \begin{equation}
    \Delta_f(e) = c(e) - f(e)
  \end{equation}
  We then augment a path by adding \(\Delta_f(P)\) to all edge flows.

  \item This, however, gives rise to a problem. The notion is too narrow, and we can hence get stuck wit a sub-optimal solution.

  \item Second idea: allow flow to decrease along some edges. This gives rise to the concept of a residual network:

\end{itemize}
\begin{definition}
  Let \(N\) be a network and \(f\) be a flow on \(N\). Define the \textbf{residual} network \(N_f\) to have
  \begin{itemize}
    \item The same vertices as \(N\)
    \item For each edge \((u, v) \in N\) with \(f(u, v) < c(u, v)\), \(N_f\) contains a ``forward'' edge \(u, v\) with capacity \(c_f(u, v) - f(u, v)\)
    \item For each edge \((u, v) \in N\) with \(f(u, v) > 0\), \(N_f\) constains a ``backward'' edge \((v, u)\) wit capacity \(c_f(v, u) = f(u, v)\).
  \end{itemize}
\end{definition}
The intuition here is that ``forward edges'' have unused capacity that can be used to push more flow from \(s\) to \(t\), whereas ``backward edges'' have surplus flow that can be \textit{redirected} topush more flow from \(s\) to \(t\).
Note that this can be considered a form of backtracking, i.e. changing our mind about previously assigned flow.

We then define the following:
\begin{definition}
  An \textbf{augmenting path} is any path from \(s\) to \(t\) in \(N_f\)
\end{definition}
\begin{definition}
  To \textbf{augment} a flow, we add \(\Delta_f(P)\) (defined as before) to forward edges and subtract it from backward edges.
\end{definition}

\subsection{Correctness of Ford-Fulkerson}

We begin with some useful definitions:
\begin{definition}
  A \textbf{cut} is a partition of \(V\) into disjoint \(V_s, V_t\) such that \(s\) is in the former and \(t\) in the latter.
  \begin{itemize}
    \item An edge \((u, v)\) with \(u \in V_s, v \in V_t\) is called ``forward''
    \item Conversely, an edge \((u, v)\) with \(u \in V_t, v \in V_s\) is called ``backward''
  \end{itemize}
\end{definition}
Note that this gives rise to two different notion of ``forward/backward'': one for \textit{residual flows/augmenting paths} and one for \textit{cuts}.
\begin{definition}
  Let \(X\) be a cut. Then
  \begin{itemize}
    \item The \textbf{capacity} of \(X\) is the sum of the capacities of the forward edges, i.e.
    \begin{equation}
      c(X) = \sum_{e \in (V_s \times V_t) \cap E}c(e)
    \end{equation}
    \item The \textbf{flow across \(X\)} is the total flow forward minus the total flow backward across the cut, i.e.
    \begin{equation}
      f(X) = \sum_{e \in (V_s \times V_t) \cap E}f(e) - \sum_{e \in (V_t \times V_s) \cap E}f(e)
    \end{equation}
  \end{itemize}
\end{definition}
We can now state the following lemmas:
\begin{lemma}
  Let \(X\) be a cut and \(f\) be a flow. Then \(f(X) \leq c(X)\)
\end{lemma}
\begin{proof}
  \begin{equation}
    f(X) = \sum_{e \in (V_s \times V_t) \cap E}f(e) - \sum_{e \in (V_t \times V_s) \cap E}f(e) \leq \sum_{e \in (V_s \times V_t) \cap E}f(e) \leq  \sum_{e \in (V_s \times V_t) \cap E}c(e) = c(x)
  \end{equation}
\end{proof}
\begin{lemma}
  Let \(X\) be a cut and \(f\) be a flow. Then \(f(X) = |f|\)
\end{lemma}
\begin{proof}
  (Intuition) all flow is ``generated'' at \(s\) and ``consumed'' at \(t\) so the value across any cut remains constant.
\end{proof}
These lemmas trivially lead to the following corollary:
\begin{corollary}
  Let \(X\) be a cut and \(f\) be a flow. Then \(|f| \leq c(X)\).
\end{corollary}
We can now proceed to the main theorem:
\begin{theorem}[Ford-Fulkerson]
  For any network \(N\) and flow \(f\), \(|f|\) is maximum (and equal to \(c(X)\) for some cut \(X\)) if and only if \(N\) has no augmenting paths
\end{theorem}
\begin{proof}
  \begin{itemize}
    \item \(\implies\): argument above
    \item \(\impliedby\): construct a cut \(X\) as follows:
    \begin{itemize}
      \item Start with \(V_s = \{s\}, V_t = V - s\)
      \item If \((u, v) \in E \cap V_s \times V_t \land f(u, v) < c(u, v)\), move \(v\) from \(V_t\) to \(V_s\)
      \item If \((u, v) \in E \cap V_t \times V_s \land f(u, v) > 0\) , then move \(u\) from \(V_t\) to \(V_s\)
      \item Repeat until no further change is possible.
    \end{itemize}
    If this process does not terminate with \(t \in V_t\), there must be an augmenting path, giving a contradiction. Hence, we obtain a cut.

    By definition of \(X\), every edge crossing \(X\) has the property that \(f(e) = c(e)\) for forward edges and \(f(e) = 0\) for backward edges. Hence, \(|f| = f(X) = c(X)\)
  \end{itemize}
\end{proof}
This leads to the following trivial corollary:
\begin{corollary}[Max-flow/min-cut theorem]
  For any network, the maximum flow value equals the minimum cut capacity
\end{corollary}
Furthermore, we have the additional property that, due to the nature of augmentation, we can prove by induction that maximum flow can always be achieved with integer flow values as long as all capacities are integer.


\end{document}
