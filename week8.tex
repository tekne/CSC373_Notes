\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{February 25 - March 1 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{corollary}{Corollary}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ptrans}[0]{\xrightarrow{\Pt}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}
\DeclareMathOperator{\Pt}{P}
\DeclareMathOperator{\NPt}{NP}
\DeclareMathOperator{\coNP}{coNP}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}

\begin{document}

\maketitle

\section{Decision Problems, \(\Pt\) and \(\NPt\)}

We begin with a definition
\begin{definition}
  A \textbf{decision problem} is a problem \(D\) where the solution is a single answer which can be either yes or no.
\end{definition}
For example,
\begin{itemize}

  \item ``Given a graph \(G = (V, E)\) and vertices \(s, t \in V\), is there a path from \(s\) to \(t\)'' is a decision problem

  \item ``Given a weighted graph \(G\) and a bound \(b\), is there a spanning tree \(T\) with total weight \(\leq b\)'' is a decision problem

  \item ``Given a weighted graph \(G\) and a bound \(b\), find a spanning tree \(T\) with total weight \(\leq b\)'' is \textit{not} a decision problem: the solution is not a single yes or no.

\end{itemize}
Why limit ourselves to decision problems? Intuitively, decision problems are ``easier'' to solve than ``search problems'' which require finding a solution or reporting none exists, since if one solves the latter, one has certainly solved the decision problem of whether a solution exists or not. Since we are interested in proving negative results, that is, that no efficient algorithm for a problem exists, showing a decision problem is hard is hence enough to show that the corresponding more general problem is also hard. It turns out, furthermore, that in most cases search problems can be solved with the help of an algorithm for the decision problem, in a way which we will later make precise.

\subsection{Formal Languages}
Recall that a formal language is a set of ``strings'', i.e. finite sequences of characters over a fixed ``alphabet, often \(\{0, 1\}\). For example, we can define the language
\begin{equation}
  \{<G, s, t> : G \text{ is a graph which contains a path from } s \text{ to } t\}
\end{equation}
with \(<G, s, t>\) representing any reasonable encoding of a graph and two of it's vertices as a string (e.g., of bits). We can draw up an equivalence between decision problems and languages as follows:
\begin{itemize}

  \item For any decision problem \(D\), define the ``language of \(D\)'' \(L_D\) to be the set of strings \(x\) such that \(x\) encodes an input to \(D\) such that the answer is ``yes''.

  \item For any language \(L\), define the ``acceptance problem for \(L\)'' \(D_L\) as follows: ``is \(x \in L\)''

\end{itemize}
With this in mind, from now on, we'll define a slight abuse of notation: \(x \in D\) means ``\(x\) is a yes-instance of \(D\)'', and \(x \notin D\) means ``\(x\) is a no-instance of \(D\)''.

\subsection{Definitions}
We can now go ahead and give formal definitions for \(\Pt\) and \(\NPt\)
\begin{definition}
  \(\Pt\) is the class of decision problems which can be solved with a polynomial time algorithm
\end{definition}
\begin{definition}
  \(\NPt\) is the class of decision problems \(D\) which can be solved by, for each possible ``certificate'' \(c\) that \(x\) is a yes-instance, ``verifying'' it in worst-case polynomial time in the size of \(x\), and returning yes if any certificate verifies, and no otherwise.
\end{definition}
While an \(\NPt\) algorithm can take exponential time, we only care that the verify phase takes polynomial time (i.e. is in \(\Pt\)). Let's now consider an example of a problem in \(\NPt\).

\subsubsection{The problem \(\proc{Composite}\)}

Consider the problem \(\proc{Composite}\): ``given any integer \(x\), does \(x\) have any factors?'' It belongs to \(\NPt\) as it is solved by a ``generate and verify'' algorithm in which the ``generate'' phase loops over all integers less than \(x\) and greater than \(1\), while the verify algorithm checks if they divide \(x\). That is, we have solution
\begin{codebox}
  \Procname{\(\proc{Composite}(x)\)}
  \li \For \(c \in \{2,...,x - 1\}\) \Do
  \li   \If \(c \equiv 0 \mod x\) \Then
  \li     \Return \(\kw{True}\) \End\End
  \li \Return \(\kw{False}\)
\end{codebox}
This works because \(\proc{Verify}(x)\), i.e. \(c \equiv 0 \mod x\), is a basic arithmetic operation and hence runs in polynomial time with respect to \(\size(x)\). The algorithm given, however, \textit{does not} run in polynomial time overall, as it runs in
\begin{equation}
  \size(x) = \log_2(x) \implies \Theta(x) = \Theta(2^{\size(x)})
\end{equation}
i.e. exponential time!

\subsubsection{Verifiers}

Why the complicated definition for \(\NPt\)? It doesn't correspond to any practical notion of computation, but it turns out that this notion captures the computational complexity of a vast majority of ``real life'' problems. Usually, problems in \(\NPt\) are defined in terms of a ``verifier'': the verification phase in the ``generate-and-verify'' algorithm. This is defined in general as:
\begin{definition}
  A \textbf{verifier} for a decision problem \(D\) is an algorithm \(V\) taking in two inputs \(x, c\) such that
  \begin{itemize}

    \item If \(x\) is a yes-instance of \(x\), then there is some string \(c\) such that \(V(x, c)\) outputs \(\kw{True}\) in polynomial time (\(c\) is called a ``certificate'')

    \item If \(x\) is not a yes-instance of \(x\), then for any string \(c\), \(V(x, c)\) outputs \(\kw{False}\) (with no restriction on runtime)

  \end{itemize}
  In other words, for any inputs \(x\), \(D(x) = \kw{True} \iff \exists c, V(x, c) = \kw{True}\) and \(V(x, c)\) runs in polynomial time measured as a function of \(n = \size(x)\) \underline{only}, ignoring the size of \(c\).
\end{definition}
Notice the asymmetry in the above definition: it is important!

\subsubsection{The problem \(\proc{Vertex-Cover}\)}

The problem \(\proc{Vertex-Cover}\) is posed as follows:
\begin{itemize}
  \item Input: given an undirected graph \(G = (V, E)\) and a positive integer \(k\)
  \item Output: Does \(G\) contain a vertex cover of size \(k\), i.e. a set \(C\) of size \(k\) such that
  \begin{equation}
    \forall (u, v) \in E, u \in C \lor v \in C
  \end{equation}
\end{itemize}
We can construct a verifier for this as follows:
\begin{codebox}
  \Procname{\(\attrib{\proc{Vertex-Cover}}{verifier}(G, k, C)\)}
  \li \If \(C \not\subseteq V \lor |C| \neq k\) \Return \kw{False}
  \li \For \((u, v) \in E\) \If \(u \notin C \land v \notin C\) \Return \kw{False}
  \li \Return \kw{True}
\end{codebox}
Note that the first step takes time \(\mc{O}(kn)\) if we don't know anything about \(C\) (and need to check it's a subset of \(V\) at all), and the second step takes time \(\mc{O}(mk)\), so in total we take time \(\mc{O}(k(m + n))\), which is polynomial. Since the verifier outputs \kw{True} if and only if by definition \(C\) is a vertex cover of \(G\) of size \(k\), then the vertex cover outputs \kw{True} for \textit{some} \(C\) if and only if \(G\) contains \textit{some} vertex cover of size \(k\).

For reference, this gives the full ``generate and verify algorithm'':
\begin{codebox}
  \Procname{\(\proc{Vertex-Cover}(G, k)\)}
  \li \For all subsets \(C \subseteq V\) of size \(k\)
  \Comment{there are \({n \choose k}\) of these} \Do
  \li   \If \(\attrib{\proc{Vertex-Cover}}{verifier}(G, k, C)\) \Return \kw{True} \End
  \li  \Return \kw{False}
\end{codebox}
\textbf{From now on, omit ``generate and verify'' and provide only verifiers.}

\subsubsection{The class \(\coNP\)}

Recall that \(D \in \NPt\) means that there is a verifier \(V(x, c)\) running in polynomial time such that
\begin{itemize}
  \item \(V(x, c) = \kw{True}\) for some \(c\) if \(x\) is a yes-instance
  \item \(V(x, c) = \kw{False}\) for all \(c\) if \(x\) is not a yes-instance
\end{itemize}
Notice the asymmetry: it is possible to verify yes-instances in polynomial time, but we don't know anything about no-instances. What about when things go the other way around, however?
\begin{definition}
  \(D \in \coNP\) iff there is a verifier \(V(x, c)\) running in polynomial time such that
  \begin{itemize}
    \item \(V(x, c) = \kw{False}\) for \textbf{some} \(c\) if \(x\) is a no-instance
    \item \(V(x, c) = \kw{True}\) for \textbf{all} \(c\) if \(x\) is not a yes-instance
  \end{itemize}
\end{definition}
Note that, if \(\lnot D\) denotes the decision problem ``\(x\) is a no-instance of \(D\)'', then \(D \in \NPt \iff \lnot D \in \coNP\). For example, since \(\proc{Composite} \in \NPt\), \(\lnot\proc{Composite} = \proc{Prime} \in \coNP\). Let's consider another example:

\subsubsection{The problem \(\proc{Dense}\)}

The problem \(\proc{Dense}\) is posed as follows:
\begin{itemize}
  \item Input: Given an undirected graph \(G\) and a positive integer \(k\)
  \item Output: Does \textit{every} subset of \(k\) vertices contain at least one edge between vertices in the subset?
\end{itemize}
We can pretty easily construct a polynomial time verifier as follows:
\begin{codebox}
  \Procname{\(\attrib{\proc{Dense}}{verifier}(G, k, c)\)}
  \li \If \(c\) is a subset of \(k\) vertices and \(G\) contains \textbf{no} edge between any two vertices in \(C\) \Then
  \li   \Return \kw{False}
  \li \Else \Return \kw{True}
\end{codebox}
Note that \(\proc{Dense}\) is the ``complement'' of \(\proc{Independent-Set}\), which can be posed as
\begin{itemize}
  \item Input: Given an undirected graph \(G\) and a positive integer \(k\)
  \item Output: Does \(G\) contain some independent set of size \(k\)?
\end{itemize}

\subsection{Relationships}

\begin{itemize}

  \item Let's start with what we do know: \(\Pt \subseteq \NPt\).

  \item But is \(\Pt \neq \NPt\)? That's a (\textit{big}) open problem in computer science!

  \item And does \(\NPt = \coNP\)? Open problem!

  \item We do know \(\Pt \subseteq \NPt \cap \coNP\), but we don't know whether \(\Pt = \NPt \cap \coNP\) either,

\end{itemize}

\section{Polynomial Time Reductions and Transformation}

Reductions and transformations formalize the notion that one problem/language is ``no harder'' than another one.
\begin{definition}
  Let \(D_1, D_2\) be decision problems with input sets \(I_1, I_2\) respectively. We say that \(D_2\) can be \textbf{reduced} to \(D_1\) in polynomial time, or \(D_1\) can be \textbf{transformed} to \(D_2\) in polynomial time, written \(D_1 \ptrans D_2\) if there is a function \(f: I_1 \to I_2\), computable in polynomial time, such that
  \begin{equation}
    \forall x \in I_1, x \in D_1 \iff f(x) \in D_2
  \end{equation}
\end{definition}
We have seen many concrete examples of reductions, for example, everything we did with network flows and linear programming. Now, however, we will apply reductions in a more abstract setting: not to try to solve problems but rather to show that a relationship exists between them.

Let's proceed with an example:
To show that \(\proc{Independent-Set} \ptrans \proc{Vertex-Cover}\), consider the function \(f\) given by \(f(G, k) = f(G, |V| - k)\). Clearly, \(f\) is computable in polynomial time, requiring only a single basic operation. Furthermore, if \(G\) contains an independent set \(S\) of size \(k\), \(V \setminus S\) is a vertex cover of size \(k\), with the converse also holding.

\begin{theorem}
If \(D_1 \ptrans D_2\), then \(D_2 \in \Pt \implies D_1 \in \Pt\) and \(D_2 \in \NPt \implies D_1 \in \NPt\).
\end{theorem}
\begin{proof}
  Let \(f: I_1 \to I_2\) be as in the definition of \(D_1 \ptrans D_2\). Since \(D_2 \in \Pt\), it follows that there is a polynomial time algorithm \(A\) such that, for all \(y \in I_2\), \(A(y) = \kw{True} \iff y \in D_2\). Hence, we obtain a polynomial time algorithm for \(D_1\) \(A(f(x))\), where \(x\) is an input to \(D_1\).
  Obviously, this answer is correct, as
  \begin{equation}
    x \in D_1 \iff f(x) \in D_2 \iff A(f(x)) = \kw{True}
  \end{equation}
\end{proof}
\begin{corollary}
  If \(D_1 \ptrans D_2\) and \(D_1 \notin \Pt\), then \(D_2 \notin \Pt\).
\end{corollary}

\subsection{\(\NPt\)-completeness}

We can use \(\ptrans\) to identify the ``hardest problems in \(\NPt\) as follows:
\begin{definition}
  A decision problem \(D\) is \textbf{\(\NPt\)-hard} iff
  \begin{equation}
    \forall D' \in \NPt, D' \ptrans D
  \end{equation}
  \(D\) is \textbf{\(\NPt\)-complete} iff \(D \in \NPt\) and \(D\) is \(\NPt-hard\) and
\end{definition}
\begin{theorem}
  If \(D\) is \(\NPt\)-complete, then \(D \in \Pt \iff \Pt = \NPt\)
\end{theorem}
\begin{proof}
  If \(\Pt = \NPt\), then \(D \in \NPt \implies D \in \Pt\). If \(D \in \Pt\), then
  \begin{equation}
    \forall D' \in \NPt, D' \ptrans D \implies D' \in P \implies \NPt \subseteq \Pt \implies \NPt = \Pt
  \end{equation}
\end{proof}
\begin{corollary}
  If \(\Pt \neq \NPt\) and \(D\) is \(\NPt\)-complete, then \(D \notin \Pt\)
\end{corollary}

\end{document}
