\documentclass{article}
\usepackage[utf8]{inputenc}

\title{CSC373 Notes}
\author{Jad Elkhaleq Ghalayini}
\date{January 28 - February 01 2019 \\
\small{Based off notes by Professor Francois Pitt}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{cancel}
\usepackage{listings}
\usepackage{qtree}

\usepackage[]{clrscode3e}

\usepackage[margin=1in]{geometry}

\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}

\newcommand{\reals}[0]{\mathbb{R}}
\newcommand{\nats}[0]{\mathbb{N}}
\newcommand{\ints}[0]{\mathbb{Z}}
\newcommand{\rationals}[0]{\mathbb{Q}}
\newcommand{\brac}[1]{\left(#1\right)}
\newcommand{\sbrac}[1]{\left[#1\right]}
\newcommand{\eval}[3]{\left.#3\right|_{#1}^{#2}}
\newcommand{\ip}[2]{\left\langle#1,#2\right\rangle}
\newcommand{\mc}[1]{\mathcal{#1}}

\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}

\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\size}{size}

\begin{document}

\maketitle

\section{All-Pairs Shortest Paths}

Let's begin this week with a problem:
\begin{itemize}

  \item Input: A directed graph \(G = (V, E)\) with integer edge weights \(w(e)\) containing no negative weight cycles

  \item Output: For each \(u, v \in V\), the length of a ``shortest'' (minimum weight) path from \(u\) to \(v\)

\end{itemize}
There are two natural ways to characterize subproblems here:
\begin{itemize}

  \item Restricting the number of edges

  \item Restricting the possible vertices on the path

\end{itemize}
We will study the latter, known as Floyd-Warshall's algorithm. Let's do it step by step, as outlined in the previous week's notes
\begin{enumerate}

  \item Finding recursive structure:

  We begin by numbering the vertices from \(1\) to \(n\). Consider a minimum-weight path \(P\) from \(u\) to \(v\). As \(G\) contains no negative weight cycles, \(P\) is simple (i.e. has no cycles). Let \(k\) be the largest intermediate vertex on the \(u, v\) path (with an intermediate vertex defined as one which is not the endpoints, or, if the path is the single edge between the endpoints, the sentinel value \(0\)).

  \begin{claim}
    \(P\) is a shortest path between \(u, k\) with intermediates in \(1,...,k - 1\) (denote this \(P_{u, v}\) followed by the shortest path between \(k, v\) with intermediates in \(1,...,k - 1\) (denote this \(P_{k, v}\)).
  \end{claim}
  \begin{proof}
    We proceed by contradiction: suppose \(P_1\) is a path from \(u\) to \(k\) with intermediates in \(1,...,k - 1\) and with a weight strictly less than that of \(P_{u, k}\).
    \begin{itemize}

      \item If \(P_1\) has no common intermediates with \(P_{k, v}\), then \(P_1 + P_{k, v}\) is a simple path from \(u\) to \(v\) with a smaller weight than \(P\), giving a contradiction.

      \item If \(P_1\) shares an intermediate with \(P_{k, v}\), denoted \(j\), then \(P_1 + P_{k, v}\) is a path with smaller weight than \(P\) containing a cycle" \(u \to j \to k \to j \to v\).

      As \(G\) contains no negative weight cycle, \(j \to k \to j\) has a non-negative weight implying \(u \to j \to v\) has a smaller weight than \(P_1 + P_{k, v}\), which itself has a smaller weight than \(P\), again giving a contradiction.

    \end{itemize}
    The proof for the other half of the path (from \(k\) to \(v\)) is similar and left to the reader.
  \end{proof}

  \item Array definition: define an \(n \times n \times n + 1\) array \(A\) such that \(A[k, u, v]\) is the minimum weight of \(u, v\) paths with intermediaries in \(1,...,k\) (where \(k\) is zero-indexed, restricting to paths with no intermediate nodes when \(k = 0\)). This then gives the recurrence relation...

  \item Recurrence relation:
  \begin{itemize}
    \item \(A[0, u, u] = 0\) since \(\delta(u, u) = 0\)
    \item \(A[0, u, v] = w(u, v)\) if \((u, v) \in E\), or \(\infty\) otherwise
    \item \(A[k, u, v] = \min\{A[ k - 1, u, v ], A[ k - 1, u, k] + A[k-1, k, v]\}\) since the minimum weight path either does not use the intermediate \(k\) or does.
  \end{itemize}

\end{enumerate}

I wanted to make sure a few things were clear about the material covered in monday's lecture, so here's a quick recap of the essential pieces of information we have so far on the problem we're working on: the all pairs shortest path problem. We're given a graph, and we want to output a table of the length of the shortest paths from one vertex to another. We're going to be using indices to refer to the vertices, which is the notation we use to define the array \(A[k, u, v]\), which is the minimum weight of any u-v path with maximum internal vertex \(\leq k\). Fo example, for \(k = 0\), we have that
\begin{equation}
  A[0, u, v] = \left\{\begin{array}{cc}
    0 & \text{if } u = v \\
    w(u, v) & \text{if} u \neq v, (u, v) \in E \\
    \infty & \text{if} u \neq v, (u, v) \notin E
  \end{array}\right.
\end{equation}
Last time, we used the recursive structure of the problem to determine the recurrence relation
\begin{equation}
  A[k, u, v] = \min(A[k - 1, u, v], A[k - 1, u, k] + A[k - 1, k, v])
\end{equation}
Why is this so? Well, the maximum vertex we're allowed to use is \(k\), so either we \textit{don't} use it (and take the shortest path from \(u, v\) with vertices all strictly less than \(k\), i.e. \(A[k - 1, u, v]\)) or we use it, going from \(u\) to \(k\) and \(k\) to \(v\).
Using this recurrence relation, we can now attempt to construct \(A[k, u, v]\) for each \(k \in \{1,...,n\}\), where \(n\) is the number of vertices, since we know \(A[0, u, v]\). Let's write down the algorithm, which it turns out is called the Floyd-Warshall algorithm:
\begin{codebox}
  \Procname{\(\proc{Floyd-Warshall}\)}
  \li \For \(u \gets 1,...,n\) \Then
  \li   \For \(v \gets 1,...,n\) \Then
  \li     \If \(u \gets v\) \Then
  \li       \(A[0, u, v] \gets 0\)
  \li     \ElseIf \((u, v) \in E\) \Then
  \li       \(A[0, u, v] \gets w(u, v)\)
  \li     \Else
  \li       \(A[0, u, v] \gets \infty\) \End
  \li    \End
  \li \For \(k \gets 1,...,n\) \Then
  \li    \For \(u \gets 1,...,n\) \Then
  \li       \For \(v \gets 1,...,n\) \Then
  \li         \If \(A[k - 1, u, v] + A[k - 1, k, v] < A[k - 1, u, v]\) \Then
  \li           \(A[k, u, v] \gets A[k - 1, u, k] + A[k - 1, k, v]\)
  \li         \Else
  \li           \(A[k, u, v] \gets A[k - 1, u, v]\)
\end{codebox}
So, running time. You can't have an easier \(\mc{O}(n^3)\) than this. Now, for a proof of correctness, all we need to prove is that it implements the recurrence, which is quite obviously true. So all we really need to know is whether the recurrence is correct, which, we know from our previous reasoning. We're not going to bother to do any kind of formal proof for these things.

Let's do two last quick things with this problem. First off, let's talk about reconstructing solutions, i.e. finding shortest paths. One way that we could do this is, as long as \(A[k, u, v] = A[k - 1, u, v]\), keep decrementing \(k\), and then, save \(k\) as one point on the path. Then, we can make recursive calls to find the paths from \(u\) to \(k\) and from \(k\) to \(v\).

The problem is we'll be re-doing work, which is bad. So let's instead to, just like for matrix chain multiplication, change \(A\) to store not just the weight but also the index on the current best path. Then we can recover paths in linear time.

To simplify notation, we'll use a new array \(B[k, u, v]\) to track the maximum vertex on any \(u, v\) path that has total weight \(A[k, u, v]\). We'll pick \(-1, 0\) as clearly invalid indices, and then, there's a very simple change to be made to the algorithm:
\begin{codebox}
  \Procname{\(\proc{Tracing-Floyd-Warshall}\)}
  \li \For \(u \gets 1,...,n\) \Then
  \li   \For \(v \gets 1,...,n\) \Then
  \li     \If \(u \gets v\) \Then
  \li       \(A[0, u, v] \gets 0\)
  \li       \(B[0, u, v] \gets -1\)
  \li     \ElseIf \((u, v) \in E\) \Then
  \li       \(A[0, u, v] \gets w(u, v)\)
  \li       \(B[0, u, v] \gets 0\)
  \li     \Else
  \li       \(A[0, u, v] \gets \infty\)
  \li       \(B[0, u, v] \gets -1\) \End
  \li    \End
  \li \For \(k \gets 1,...,n\) \Then
  \li    \For \(u \gets 1,...,n\) \Then
  \li       \For \(v \gets 1,...,n\) \Then
  \li         \If \(A[k - 1, u, v] + A[k - 1, k, v] < A[k - 1, u, v]\) \Then
  \li           \(A[k, u, v] \gets A[k - 1, u, k] + A[k - 1, k, v]\)
  \li           \(B[k, u, v] \gets k\)
  \li         \Else
  \li           \(A[k, u, v] \gets A[k - 1, u, v]\)
  \li           \(B[k, u, v] \gets B[k - 1, u, v]\)
\end{codebox}
Now, once this is done, if we want to know the best path from \(u\) to \(v\). Before this, let me make one observation. Right now, the way I've done this, the algorithm gets a little messy with various indices. Writing the algorithm with the current set-up is a good exercise. But let's try to simplify things.

First of all, conceptually, we're done. We've solved the problem. In practice, thinking a little bit about the complexity of this algorithm, while we might not be able to get rid of the \(\Theta(n^3)\) runtime, we are storing two different 3D array here. We need the 2D apsect, since that's the size of the result, but what about all the different values of \(k\)? Do we need all those different values of \(k\) in the end? Can we do better than \(\Theta(n^3)\) space?

So I want to give you an observation: it turns out we can do much better. Now from the point of view of this course, we're done: we've got a polynomial time algorithm. But just taking it a step beyond, to show we can improve on the space quite significantly, we can get it down to \(\mc{O}(n^2)\). The way we proceed is as follows:

I've already pointed out that if we've got all the values in the 2D slice \(A[k]\), we know that every value in the next slice is based off only the previous slice. So we never refer to any values earlier than the values in \(A[ k - 1]\). So right there, there's one super easy trick: instead of keeping track of \(n\) different layers, just keep track of 2. That would be messy though, even though we could do better than just copying over and over again, via swapping. We can instead do something more clever than that, with the following observation:

If I look at a particular cell \((u, v)\) in our \(k\) slice, this cell will depend on the value at index \((u, v)\) in the previous slice. It also depends on the value at \((u, k)\) and \((k, v)\). So it depends on 3 values in the previous slice. This repeats for every pair \((u, v)\).

The key observation is that, when I'm doing this update, what happens when I'm updating a value already at index \(k\), say in the column \((*, k)\) or row \((k, *)\). Well, it's only going to depend on the value \((k, k)\) (which is always 0) and itself. So the values in the \(k^{th}\) row and column are always going to stay the same.

The trick we're going to use is that, instead of keeping two slices, we're just going to keep one current copy of the 2D array, and we're going to loop and update the values directly in place. Put another way, we're going to eliminate the index \(k\) from the array \underline{in the algorithm}. This works because for each dependency \((u, v\), the only dependency that will be changed, namely \((u, v)\), is exactly the thing being updated.

The best part is is we can do the same thing for \(B\), using the same logic. And now we've reduced the space complexity to \(\mc{O}(n^2)\).

The reason why we can reconstruct the best path from that last slice \(B\) is because recursively we will already have all the information we need.

\section{Transitive Closure}

Now, let's tackle another dynamic programming problem: given a directed graph \(G = (V, E)\), determine whether or not each pair of vertices \(u, v\) has an edge between them.

We already know a few good algorithms for this:
\begin{itemize}
  \item Run BFS \(n\) times
  \item Run DFS \(n\) times
  \item Use a shortest-paths algorithm with all weights set to 1 (or really any weight configuration)
\end{itemize}
Let's explore a different approach, however, which sheds light on a few interesting techniques. This time, let's start with the (binary) \textit{adjacency matrix}\ \(A\) with additional \(1\)'s on the diagonal (since it is always possible to reach a vertex \(v\) from itself). Now, consider the matrix \(A^2\). What does the entry in the \(i, j\) position represent?

Well,
\[A^2_{ij} = \sum_{k = 1}^nA_{ik}A_{kj}\]
Since \(A\) is positive, it follows that
\[A^2_{ij} = 0 \iff \forall k \in \{1,...,n\}, A_{ik} = 0 \lor A_{kj} = 0\]
i.e. a vertex is missing between \((i, k)\) or between \((k, j)\) for every \(k\), or, put another way, there is no path of length \(\leq 2\) between \(i\) and \(j\). This generalizes so that entries of \(A^3\) contain information of paths up to length \(3\), and so on.

\begin{itemize}

  \item Let's simplify our computation a bit with a trick: replace multiplication by conjunction and addition by disjunction, so each \(A^n\) remains binary, and is hence true (\(1\)) if and only if there \textit{is} a path of length \(\leq n\) between \(i\) and \(j\).
The advantage here is that storing a boolean matrix is more space efficient than storing a numerical matrix \textit{and} boolean operations are faster than arithmetic operations.

  \item Let's use another trick: instead of computing each power of \(A\) one by one, use repeated squaring, i.e.
  \[(A^2)^2 = A^4, (A^4)^2 = A^8, ...\]
  We can then reach \(A^m\) such that \(m \geq n\) in \(\log n\) steps. Hence, we obtain runtime \(\Theta(n^3\log n)\) (\(\log n\) \(n^3\) matrix multiplications.)

  \item Let's apply \textit{yet another} trick: we can use divide-and-conquer to speed up matrixmultiplication to \(\mc{O}(n^{\log_27}) = \mc{O}(n^{2.8...})\), specifically using Strassen's algorithm. So we get time \(\Theta(n^{2.81}\log n)\).

\end{itemize}



\end{document}
